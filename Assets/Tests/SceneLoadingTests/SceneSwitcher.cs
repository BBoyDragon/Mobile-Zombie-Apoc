using Assets.Scripts.SceneLoader;
using UnityEngine;

public class SceneSwitcher : MonoBehaviour
{
    [SerializeField] SceneLoadData _scenLoadData;

    private SceneLoadingHandler _sceneLoadingHandler;

    private void Start()
    {
        _sceneLoadingHandler = new SceneLoadingHandler(_scenLoadData, 
            SceneLoaderCommunicatorKeeper.GetContext());
    }

    void OnCollisionEnter(Collision myCollision)
    {
        _sceneLoadingHandler.LoadNextScene();
    }

    private void Update()
    {
        System.Random rnd = new System.Random();

        int i = rnd.Next(100);

        if (i > 98)
        {
            _sceneLoadingHandler.LoadMenu();
        }

    }
}
