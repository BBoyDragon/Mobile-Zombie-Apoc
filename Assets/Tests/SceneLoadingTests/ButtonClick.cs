using Assets.Scripts.SceneLoader;
using UnityEngine;

public class ButtonClick : MonoBehaviour
{
    [SerializeField] SceneLoadData _scenLoadData;

    private SceneLoadingHandler _sceneLoadingHandler;

    private void Start()
    {
        _sceneLoadingHandler = new SceneLoadingHandler(_scenLoadData,
            SceneLoaderCommunicatorKeeper.GetContext());
    }

    public void OnClick()
    {
        _sceneLoadingHandler.LoadNextScene();
    }
}
