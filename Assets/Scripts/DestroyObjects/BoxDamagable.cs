﻿using Enemy;
using UnityEngine;

namespace DestroyObject
{
    public class BoxDamagable : MonoBehaviour, IDamagable
    {
        [SerializeField]
        private int restHit = 2;
        [SerializeField]
        private int ID;
        private void Start()
        {
            ID = gameObject.GetInstanceID();
        }
        public void Hit(int dmg)
        {
            restHit--;
#if UNITY_EDITOR
            Debug.Log($"Box {ID} is damaged {restHit}");
#endif
            if (restHit <= 0)
                Destroy(gameObject);
        }
    }
}
