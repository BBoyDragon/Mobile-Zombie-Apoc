﻿using Enemy;
using UnityEngine;

namespace DestroyObject
{
    public class SlimWallDamagable : MonoBehaviour, IDamagable
    {
        [SerializeField]
        private int restHit = 5;
        [SerializeField]
        private int ID;
        private void Start()
        {
            ID = gameObject.GetInstanceID();
        }
        public void Hit(int dmg)
        {
            restHit--;
#if UNITY_EDITOR
            Debug.Log($"SlimWall {ID} is damaged {restHit}");
#endif
            if (restHit <= 0)
                Destroy(gameObject);
        }
    }
}
