﻿using Enemy;
using UnityEngine;

namespace DestroyObject
{
    internal class WindowDamagable : MonoBehaviour, IDamagable
    {
        [SerializeField]
        private int restHit = 1;
        [SerializeField]
        private int ID;
        private void Start()
        {
            ID = gameObject.GetInstanceID();
        }
        public void Hit(int dmg)
        {
            restHit--;
#if UNITY_EDITOR
            Debug.Log($"Window {ID} is damaged {restHit}");
#endif
            if (restHit <= 0)
                Destroy(gameObject);
        }
    }
}
