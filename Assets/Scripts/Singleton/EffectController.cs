﻿using Data;
using UnityEngine;

namespace Singleton
{
    public class EffectController
    {
        private static EffectController _instance;
        private EffectSystemData _data;

        public static EffectController Instance => _instance ??= new EffectController();

        public void SetEffectSystemData(EffectSystemData data)
        {
            _data = data;
        }

        public void ActiveLevelUpPlayerEffect(Transform parent)
        {
            Object.Instantiate(_data.LevelUpEffect, parent.transform);
        }
    }
}
