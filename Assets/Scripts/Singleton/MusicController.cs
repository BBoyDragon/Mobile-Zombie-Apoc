﻿using Data;

namespace Singleton
{
    public class MusicController
    {
        private static MusicController _instance;
        private MusicData _data;

        public static MusicController Instance => _instance ??= new MusicController();

        public void SetMusicData(MusicData data)
        {
            _data = data;
        }
        
        
    }
}