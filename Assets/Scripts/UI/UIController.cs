﻿using Data;
using Player;
using TMPro;
using UnityEngine;

namespace UI
{
    public class UIController
    {
        private UiData _data;
        public PlayerControlView PlayerControlView { get; private set; }
        public ProgressbarView ExProgressbarView { get; private set; }

        public AttackMeleeView AttackMeleeView { get; private set; }
        public Canvas mainCanvas { get; private set; }

        public TMP_Text AmoCountText { get; private set; }


        public UIController(UiData data)
        {
            _data = data;
        }

        public void Spawn()
        {
            var canvas = Object.Instantiate(_data.UICanvas);

            PlayerControlView = Object.Instantiate(_data.PlayerControlView, canvas.transform);
            ExProgressbarView = Object.Instantiate(_data.ExpProgressbarPrefab, canvas.transform);

            AttackMeleeView = Object.Instantiate(_data.AttackMeleePrefab, canvas.transform);
            mainCanvas = canvas;
            AmoCountText = Object.Instantiate(_data.AmoCountText, canvas.transform);

        }
    }
}