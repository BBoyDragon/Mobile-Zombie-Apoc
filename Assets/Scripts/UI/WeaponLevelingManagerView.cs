﻿using System.Linq;
using UnityEngine;

namespace WeaponLevelingSystem
{
    public class WeaponLevelingManagerView : MonoBehaviour
    {
        [SerializeField]
        private Transform _panelForLevelingCards;

        [SerializeField]
        private WeaponLevelingCardView _weaponLevelingCardView;

        private WeaponLevelingCardView[] _instatceCards;

        private const int _levelingCardCount = 3;

        private Player.Player _player;

        public System.Action OnWorkIsDone;
        private void ActivateBonus(WeaponLevelingEffect weaponLevelingEffect)
        {
            weaponLevelingEffect.Apply(_player);

            DestroyAll();
        }

        private void DestroyAll()
        {
            for (int i = 0; i < _instatceCards.Length; i++)
            {
                _instatceCards[i].OnClick -= ActivateBonus;
                Object.Destroy(_instatceCards[i].gameObject);
            }

            _instatceCards = null;

            OnWorkIsDone?.Invoke();
        }

        public void Init(Player.Player player)
        {
            _player = player;
        }
        public void SetBonusCard(WeaponLevelingEffect[] weaponLevelingEffect)
        {
            _instatceCards = new WeaponLevelingCardView[_levelingCardCount];
            var randomElements = GetRandomElements(weaponLevelingEffect, _levelingCardCount);

            for (int i = 0; i < _levelingCardCount; i++)
            {
                _instatceCards[i] = Object.Instantiate(_weaponLevelingCardView, _panelForLevelingCards.transform);
                _instatceCards[i].SetBonusEffect(randomElements[i]);
                _instatceCards[i].OnClick += ActivateBonus;
            }
        }

        private WeaponLevelingEffect[] GetRandomElements(WeaponLevelingEffect[] array, int count)
        {
            return array.OrderBy(arg => System.Guid.NewGuid()).Take(_levelingCardCount).ToArray();
        }

    }
}
