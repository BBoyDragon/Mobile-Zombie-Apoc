﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class AttackMeleeView : MonoBehaviour
    {
        [SerializeField] private Button button;

        public Button Button => button;
    }
}
