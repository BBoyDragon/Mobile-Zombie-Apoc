using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ProgressbarView : MonoBehaviour
    {
        [SerializeField] private Image progressBar;
        [SerializeField] private TMP_Text _text;

        public Image ProgressBar => progressBar;

        public TMP_Text Text => _text;
    }
}