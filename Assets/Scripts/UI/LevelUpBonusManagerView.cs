using System.Linq;
using UnityEngine;

namespace LevelUpBonusSystem
{
    public class LevelUpBonusManagerView : MonoBehaviour
    {
        [SerializeField]
        private Transform _panelForBonusCards;

        [SerializeField]
        private LevelUpBonusCardView _bonusCards;

        private LevelUpBonusCardView[] _instatceCards;

        private const int bonusCardCount = 3;

        private Player.Player _player;

        public System.Action OnWorkIsDone;
        private void ActivateBonus(BonusEffect bonusEffect)
        {
            bonusEffect.Apply(_player);

            DestroyAll();
        }

        private void DestroyAll()
        {
            for (int i = 0; i < _instatceCards.Length; i++)
            {
                _instatceCards[i].OnClick -= ActivateBonus;
                Object.Destroy(_instatceCards[i].gameObject);
            }

            _instatceCards = null;

            OnWorkIsDone?.Invoke();
        }

        public void Init(Player.Player player)
        {
            _player = player;
        }
        public void SetBonusCard(BonusEffect[] bonusEffect)
        {
            _instatceCards = new LevelUpBonusCardView[bonusCardCount];
            var randomElements = GetRandomElements(bonusEffect, bonusCardCount);

            for (int i = 0; i < bonusCardCount; i++)
            {
                _instatceCards[i] = Object.Instantiate(_bonusCards, _panelForBonusCards.transform);
                _instatceCards[i].SetBonusEffect(randomElements[i]);
                _instatceCards[i].OnClick += ActivateBonus;
            }
        }

        private BonusEffect[] GetRandomElements(BonusEffect[] array, int count)
        {
            return array.OrderBy(arg => System.Guid.NewGuid()).Take(bonusCardCount).ToArray();
        }


    }
}
