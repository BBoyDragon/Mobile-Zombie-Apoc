﻿using System;
using DefaultNamespace;
using DG.Tweening;
using UI;
using UnityEngine;
using UnityEngine.AI;

namespace Enemy
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class EnemyView : MonoBehaviour, IAimable, IDamagable
    {
        private Enemy _enemy;
        private ProgressbarView _aimView;
        private ProgressbarView _healthBarView;

        private bool canSee;

        public float dis;
        public event Action<float> OnHit;
        public bool CanAim()
        {
            return !_enemy.HealthManager.IsDead;
        }

        public event Action OnShootReady;
      

        public ProgressbarView HealthBarView => _healthBarView;
        public bool CanSee => canSee;

        public void LockTarget()
        {
            _aimView.ProgressBar.fillAmount = 1;
            _aimView.gameObject.SetActive(true);
            OnShootReady?.Invoke();
        }

        public void HideUI()
        {
            _aimView.ProgressBar.DOKill();
            _aimView.ProgressBar.fillAmount = 0f;
            _aimView.gameObject.SetActive(false);
        }

        public void ShowUI(float aimTime)
        {
            _aimView.gameObject.SetActive(true);
            _aimView.ProgressBar.DOFillAmount(1f, aimTime).OnComplete(() => OnShootReady?.Invoke());
        }


        public void SetEnemy(Enemy enemy, ProgressbarView aimView, ProgressbarView healthBarView)
        {
            _enemy = enemy;
            _aimView = aimView;
            _healthBarView = healthBarView;
        }
        
        public void SetVisible(bool visible)
        {
            canSee = visible;
            
            _healthBarView.gameObject.SetActive(visible);

            var enemyMeshRenders = GetComponents<MeshRenderer>();
            for (var i = 0; i < enemyMeshRenders.Length; i++)
            {
                enemyMeshRenders[i].enabled = visible;
            }
        }
#if UNITY_EDITOR


        private void Update()
        {
            if (_enemy.Agent.Target != null)
            {
                Debug.DrawLine(transform.position, _enemy.Agent.TargetPoint, Color.red);
                dis = Vector3.Distance(transform.position, _enemy.Agent.Target.transform.position);

                // для быстрого теста смерти
            
            }

        }
#endif
        public Transform GetTarget()
        {
            return transform;
        }

        public string GetName()
        {
            return _enemy.Data.Name;
        }

        public void Hit(int dmg)
        {
            OnHit?.Invoke(dmg);
            Debug.Log($"{gameObject.name} {gameObject.GetInstanceID()} is damaged {dmg}");
        }
        
    }
}