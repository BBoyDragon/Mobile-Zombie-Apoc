﻿using System;

namespace Enemy
{
    public interface ITrophyGiver
    {
        public event Action<ITrophyGiver, int> OnGiveTrophy;
    }
}