﻿using System;

namespace Enemy
{
    public interface IHealthManager
    {
        float Health { get; }
        void ApplyDamage(float damage);
        bool IsDead { get; }
        event Action OnDead;
    }
}
