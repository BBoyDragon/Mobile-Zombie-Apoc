﻿using System;
using System.Collections.Generic;
using Data;
using Runtime.Controller;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Enemy
{
    public class EnemySpawnController 
    {
        private EnemiesData _data;
        private FogOfWarData _fogOfWarData;
        private List<EnemySpawnView> _spawnPoses;
        private List<Enemy> _enemies;
        public event Action<Enemy> OnEnemySpawned;

        public EnemySpawnController(EnemiesData data, List<EnemySpawnView> spawnPoses, FogOfWarData fogOfWarData)
        {
            _data = data;
            _spawnPoses = spawnPoses;
            _enemies = new List<Enemy>();
            _fogOfWarData = fogOfWarData;
        }

        public void SpawnEnemies()
        {
            _spawnPoses.ForEach(SpawnEnemiesOnSpawnLocation);
            
        }
        
        private void SpawnEnemiesOnSpawnLocation(EnemySpawnView spawnView)
        {
            var enemyCount = 0;
            for (int i = 0; i < spawnView.EnemyCount; i++)
            {
                var enemyInfo = _data.Enemies[Random.Range(0, _data.Enemies.Count - 1)];
                Enemy enemy = new Enemy(enemyInfo);
                _enemies.Add(enemy);

                enemy.Spawn(spawnView, _fogOfWarData.ViewAreaEnabled, enemyCount++);
                OnEnemySpawned?.Invoke(enemy);
            }
        }

        public List<IExpGiver> GetExpGivers()
        {
            var res = new List<IExpGiver>(_enemies.Count);
            _enemies.ForEach(enemy => res.Add(enemy));
            return res;
        }
    }
}