﻿namespace Enemy
{
    public interface IDamagable
    {
        public void Hit(int dmg);
    }
}