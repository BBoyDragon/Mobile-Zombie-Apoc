﻿using System;
using AI;
using Data;
using DefaultNamespace;
using StateMachine.States;
using UI;
using StatsManager;
using UnityEngine;
using UnityEngine.AI;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Enemy
{
    public class Enemy : IExpGiver, ITrophyGiver
    {
        #region States

        private AgressiveState _agressiveState;
        private Patrol _patrolState;
        private AttackState _attackState;
        private DeathState _deathState;

        public AgressiveState AgressiveState => _agressiveState;

        public Patrol PatrolState => _patrolState;

        public AttackState AttackState => _attackState;
        public DeathState DeathState => _deathState;

        #endregion

        private EnemyView _enemyView;
        private EnemyInfo _data;
        private AIAgent _agent;
        private BotStateMachine _botStateMachine;
        private IHealthManager _healthManager;
        private ProgressbarView _healthBarView;
        public BotStateMachine BotStateMachine => _botStateMachine;
        public event Action<IExpGiver, int> OnGiveExp;
        public event Action<ITrophyGiver, int> OnGiveTrophy;

        public AIAgent Agent => _agent;

        public EnemyView EnemyView => _enemyView;

        public EnemyInfo Data => _data;

        public event Action<Enemy> OnDead;

        public IHealthManager HealthManager => _healthManager;


        public Enemy(EnemyInfo data)
        {
            _data = data;
        }

        public void Spawn(EnemySpawnView spawnView, bool fieldOfViewEnabled, int priority)
        {
            Vector3 spawnPos = new Vector3(
                Random.Range(spawnView.RectTransform.position.x - spawnView.RectTransform.rect.width / 2,
                    spawnView.RectTransform.position.x + spawnView.RectTransform.rect.width / 2),
                spawnView.RectTransform.position.y,
                Random.Range(spawnView.RectTransform.position.z - spawnView.RectTransform.rect.height / 2,
                    spawnView.RectTransform.position.z + spawnView.RectTransform.rect.height / 2));

            var go = Object.Instantiate(_data.EnemyPrefab, spawnPos, Quaternion.identity);
            var navMeshAgent = go.GetOrAddComponent<NavMeshAgent>();
            _agent = new AIAgent(this, navMeshAgent);
            _enemyView = go.GetOrAddComponent<EnemyView>();


            _healthBarView = Object.Instantiate(_data.HealthBarView, _enemyView.transform);
            _healthManager = new HealthManager(_data.Health, _healthBarView);
            _healthManager.OnDead += Death;
            _enemyView.OnHit += _healthManager.ApplyDamage;

            _enemyView.SetEnemy(this, Object.Instantiate(_data.AimTimerView, _enemyView.transform),
                _healthBarView);
            
            if (fieldOfViewEnabled)
            {
                _enemyView.SetVisible(false);
            }

            _enemyView.HideUI();
            navMeshAgent.avoidancePriority = priority;
            _botStateMachine = new BotStateMachine();
            _agressiveState = new AgressiveState(this, _botStateMachine);
            _attackState = new AttackState(this, _botStateMachine);
            _patrolState = new Patrol(this, _botStateMachine);
            _deathState = new DeathState(this, _botStateMachine);
            _botStateMachine.Initialize(_patrolState);
        }

        private void Death()
        {
            if (!HealthManager.IsDead)
                return;
            
            OnGiveExp?.Invoke(this, _data.ExpCountOnDeath);
            OnGiveTrophy?.Invoke(this, _data.Trophy.GetInstanceID());
            OnDead?.Invoke(this);
        }
    }
}