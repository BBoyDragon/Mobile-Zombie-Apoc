﻿using System;
using UnityEngine;

namespace Enemy
{
    public interface IExpGiver
    {
        public event Action<IExpGiver, int> OnGiveExp;
    }
}