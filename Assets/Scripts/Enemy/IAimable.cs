﻿using System;
using UnityEngine;

namespace Enemy
{
    public interface IAimable
    {
        public bool CanSee { get; }
        
        public Transform GetTarget();
        public string GetName();
        bool CanAim();
        event Action OnShootReady;
        void LockTarget();
        void HideUI();
        void ShowUI(float aimTime);
    }
}