using StatsManager;

namespace Player
{
    public class PlayerStatsManager
    {
        private HealManager _healManager;
        private HealthManager _healthManager;
        private AccuracyManager _accuracyManager;
        private CritManager _critManager;
        private CaptureSpeedManager _captureSpeedManager;
        private AmmunitionManager _ammunitionManager;
        private DodgeChanceManager _dodgeChanceManager;
        private ReloadSpeedManager _reloadSpeedManager;
        private SpeedManager _speedManager;
        private AdditionalExpManager _additionalExpManager;
        private ArmorManager _armorManager;

        public PlayerStatsManager(Data.PlayerData playerData, UI.ProgressbarView healthBarView)
        {
            _healManager = new HealManager(playerData.HealthRegeneration, playerData.HealingGain);
            _healthManager = new HealthManager(playerData.Health, healthBarView);
            _accuracyManager = new AccuracyManager(playerData.Accuracy);
            _critManager = new CritManager(playerData.CritChance, playerData.CritPower);
            _captureSpeedManager = new CaptureSpeedManager(playerData.TargetCaptureSpeed);
            _ammunitionManager = new AmmunitionManager(playerData.Ammunition);
            _dodgeChanceManager = new DodgeChanceManager(playerData.DodgeChance);
            _reloadSpeedManager = new ReloadSpeedManager(playerData.ReloadSpeed);
            _speedManager = new SpeedManager(playerData.Speed);
            _additionalExpManager = new AdditionalExpManager(playerData.AdditionalExperience);
            _armorManager = new ArmorManager(playerData.Armor);
        }

        public HealManager HealManager => _healManager;
        public HealthManager HealthManager => _healthManager;
        public AccuracyManager AccuracyManager => _accuracyManager;
        public CritManager CritManager => _critManager;
        public CaptureSpeedManager CaptureSpeedManager => _captureSpeedManager;
        public AmmunitionManager AmmunitionManager => _ammunitionManager;
        public DodgeChanceManager DodgeChanceManager => _dodgeChanceManager;
        public ReloadSpeedManager ReloadSpeedManager => _reloadSpeedManager;
        public SpeedManager SpeedManager => _speedManager;
        public AdditionalExpManager AdditionalExpManager => _additionalExpManager;
        public ArmorManager ArmorManager => _armorManager;

    }
}
