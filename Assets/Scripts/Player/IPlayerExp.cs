﻿using System;

namespace Player
{
    public interface IPlayerExp
    {
        event Action<int> OnExpChanged;
        
        int AddExp(int count);
        int GetExp();

        void ResetExp();
    }
}