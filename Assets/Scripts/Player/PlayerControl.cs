﻿using Controller;
using DG.Tweening;
using Enemy;
using Runtime.Controller;
using UI;
using UnityEngine;
using Weapon;


namespace Player
{
    public class PlayerControl : IFixedExecute, IExecute
    {
        private PlayerControlView _playerControlView;
        private PlayerView _playerView;
        private float _speed;
        private Player _player;
        private IAimable _target;

        private WeaponMelee.WeaponMeleeView _weaponMeleeView;

        private WeaponControl weaponControl;

        private readonly Camera _camera;


        public WeaponControl WeaponControl => weaponControl;

        public PlayerControl(PlayerControlView playerControlView, Player player)
        {
            _playerControlView = playerControlView;
            _player = player;


            _camera = Camera.main;
        }


        private void ChangeWeaponMelee(WeaponMelee.WeaponMelee weaponMelee)
        {
            _weaponMeleeView.SetUpWeapon(weaponMelee);
        }

        public void SetAttackMeleeView(AttackMeleeView attackMeleeView)
        {
            _weaponMeleeView = Object.Instantiate(
                _player.Data.WeaponMelee.WeaponMeleeView,
                _playerView.transform.position + _playerView.transform.forward * 1.5f,
                Quaternion.identity,
                _playerView.transform);
            ChangeWeaponMelee(_player.Data.WeaponMelee);
            _weaponMeleeView.gameObject.SetActive(false);
            _weaponMeleeView.SetPlayerID(_playerView.gameObject.GetInstanceID());
            attackMeleeView.Button.onClick.AddListener(_weaponMeleeView.Attack);
        }


        private void Rotate(Vector2 dir, float time = 0.3f)
        {
            if (dir.Equals(Vector2.zero)) return;


            _playerView.transform.DOKill();
            _playerView.transform.DOLookAt(_playerView.transform.position +
                                           new Vector3(dir.normalized.x, 0f, dir.normalized.y), time);
        }


        public void SetPlayer(PlayerView playerView, float speed)
        {
            _playerView = playerView;
            weaponControl = new WeaponControl(_player.Data.Weapon, _player.ReloadBarView);
            _speed = speed;
        }

        public void FixedExecute()
        {
            _playerView.Rigidbody.velocity = new Vector3(_playerControlView.PlayerMove.Direction.x * _speed,
                _playerView.Rigidbody.velocity.y,
                _playerControlView.PlayerMove.Direction.y * _speed);
            if (!weaponControl.LockTarget(_playerView.transform))
                Rotate(_playerControlView.PlayerView.Direction);
            else
            {
                var dir = (weaponControl.Target.GetTarget().position - _playerView.transform.position).normalized;

                Rotate(Vector3.Angle(_playerControlView.PlayerView.Direction, _playerView.transform.forward) > 30
                    ? _playerControlView.PlayerView.Direction
                    : new Vector2(dir.x, dir.z));
            }

#if UNITY_EDITOR
            if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
            {
                _playerView.Rigidbody.velocity =
                    new Vector3(Input.GetAxis("Horizontal") * _speed, 0,
                        Input.GetAxis("Vertical") * _speed);
            }

            var point = (_camera.ScreenPointToRay(Input.mousePosition).GetPoint(20) -
                         _playerView.transform.position).normalized;


            if (!weaponControl.LockTarget(_playerView.transform))
                Rotate(new Vector2(point.x, point.z));
            else
            {
                var dir = (weaponControl.Target.GetTarget().position - _playerView.transform.position).normalized;

                Rotate(Vector2.Angle(new Vector2(point.x, point.z),
                    new Vector2(_playerView.transform.forward.x, _playerView.transform.forward.z)) > 30
                    ? new Vector2(point.x, point.z)
                    : new Vector2(dir.x, dir.z));
            }
#endif
        }

        public void Execute()
        {
            _player.HealthBarView.transform.LookAt(_camera.transform);
        }
    }
}