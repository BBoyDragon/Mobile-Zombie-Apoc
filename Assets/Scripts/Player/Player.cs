﻿using Data;
using DefaultNamespace;
using DG.Tweening;
using Enemy;
using Runtime.Controller;
using StatsManager;
using UI;
using UnityEngine;

namespace Player
{
    public class Player
    {
        private PlayerData _data;
        private PlayerView _instance;
        private PlayerControl _playerControl;
        private IPlayerExpControl _playerExpControl;
        private PlayerStatsManager _playerStatsManager;
        private ProgressbarView _healthBarView;
        public PlayerData Data => _data;

        public ProgressbarView HealthBarView => _healthBarView;

        public IPlayerExpControl PlayerExpControl => _playerExpControl;
        public PlayerStatsManager PlayerStatsManager => _playerStatsManager;
        private ProgressbarView _reloadBarView;

        public GameObject Instance => _instance.gameObject;


        public ProgressbarView ReloadBarView => _reloadBarView;
        

        public System.Action<Player> OnLevelUp;

        public PlayerControl PlayerControl => _playerControl;

        public Player(PlayerData data, PlayerControlView playerControlView, PlayerExpObservable _playerExpObservable)

        {
            _data = data;
            _playerExpControl = new PlayerExpControl(_playerExpObservable);
            _playerControl = new PlayerControl(playerControlView, this);
        }
        

        public void Spawn(Transform spawnPos, AttackMeleeView attackMeleeView)
        {
            var go = Object.Instantiate(_data.Prefab, spawnPos.position + _data.Prefab.transform.position,
                spawnPos.rotation);
            _instance = go.AddComponent<PlayerView>();

            _instance.OnLevelUp += LevelUp;

            _healthBarView = Object.Instantiate(_data.HealthBarView, _instance.transform);
            _reloadBarView = Object.Instantiate(_data.ReloadBarView, _instance.transform);
            _playerStatsManager = new PlayerStatsManager(_data, _healthBarView);

            PlayerStatsManager.HealthManager.OnDead += Death;
            _instance.OnHit += PlayerStatsManager.HealthManager.ApplyDamage;
            _instance.Rigidbody.freezeRotation = true;
            _playerControl.SetPlayer(_instance, _playerStatsManager.SpeedManager.Speed);
            _playerControl.SetAttackMeleeView(attackMeleeView);
        }

        public IController GetController()
        {
            return _playerControl;
        }


        public void LevelUp()
        {
            OnLevelUp?.Invoke(this);
        }

        public IController GetExpController()
        {
            return _playerExpControl;
        }

        private void Death()
        {
            if (!PlayerStatsManager.HealthManager.IsDead) return;
            Debug.Log("Помер");
            PlayerStatsManager.HealthManager.ApplyDamage(-_data.Health);
        }
    }
}