﻿using System;
using System.Collections.Generic;
using Enemy;
using Runtime.Controller;

namespace Player
{
    public class PlayerExpObservable: ICleanup
    {
        public event Action<int> OnGiveExp;

        private readonly List<IExpGiver> _callers;

        public PlayerExpObservable()
        {
            _callers = new List<IExpGiver>();
        }
        
        public void AddСaller(IExpGiver expGiver)
        {
            expGiver.OnGiveExp += GiveExp;
            _callers.Add(expGiver);
        }
 
        public void RemoveСaller(IExpGiver expGiver)
        {
            expGiver.OnGiveExp -= GiveExp;
            _callers.Remove(expGiver);
        }
 
        private void GiveExp(IExpGiver expGiver, int expCount)
        {
            RemoveСaller(expGiver);
            OnGiveExp?.Invoke(expCount);
        }

        public void Cleanup()
        {
            foreach (var expGiver in _callers)
            {
                expGiver.OnGiveExp -= GiveExp;
            }
            _callers.Clear();
        }
    }
}