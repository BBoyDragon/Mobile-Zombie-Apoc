﻿using System;
using DefaultNamespace;
using Enemy;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(Rigidbody), typeof(CapsuleCollider))]
    public class PlayerView : MonoBehaviour, IDamagable
    {
        private Rigidbody _rigidbody;
        private CapsuleCollider _capsuleCollider;

        public event Action<float> OnHit;
        [SerializeField]
        private bool _levelUp;
        public event Action OnLevelUp;

        private void Update()
        {
            if (_levelUp)
            {
                _levelUp = false;
                OnLevelUp?.Invoke();               
            }
        }
        public Rigidbody Rigidbody
        {
            get
            {
                if (_rigidbody == null)
                {
                    _rigidbody = gameObject.GetOrAddComponent<Rigidbody>();
                }

                return _rigidbody;
            }
        }

        public CapsuleCollider CapsuleCollider
        {
            get
            {
                if (_capsuleCollider == null)
                {
                    _capsuleCollider = gameObject.GetOrAddComponent<CapsuleCollider>();
                }

                return _capsuleCollider;
            }
        }
        

        public void Hit(int dmg)
        {
            OnHit?.Invoke(dmg);
            Debug.Log($"Player is damaged {dmg}");
        }
    }
}