﻿using System;

namespace Player
{
    public class PlayerExp: IPlayerExp
    {
        private int _expCount;
        
        public event Action<int> OnExpChanged;

        public int AddExp(int count)
        {
            _expCount += count;
            OnExpChanged?.Invoke(count);
            
            return _expCount;
        }

        public int GetExp()
        {
            return _expCount;
        }

        public void ResetExp()
        {
            _expCount = 0;
            OnExpChanged?.Invoke(_expCount);
        }
    }
}