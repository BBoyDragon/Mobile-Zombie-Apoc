﻿using System;
using Data;
using DG.Tweening;
using Runtime.Controller;
using UI;

namespace Player
{
    public class PlayerExpControl: IPlayerExpControl
    {
        private int _expCount = 0;
        private ProgressbarView _progressbarView;
        private readonly PlayerExpObservable _playerExpObservable;
        
        public event Action<int> OnExpChanged;

        public PlayerExpControl(PlayerExpObservable playerExpObservable)
        {
            _playerExpObservable = playerExpObservable;
        }
        
        public void AddExp(int count)
        {
            _expCount += count;
            OnExpChanged?.Invoke(count);
            _progressbarView.ProgressBar.DOFillAmount((float)_expCount / 100, 0.3f);
            _progressbarView.Text.text = $"{_expCount}/100";
        }

        public void SetProgressbar(ProgressbarView progressbar)
        {
            _progressbarView = progressbar;
            _progressbarView.ProgressBar.DOFillAmount((float)_expCount / 100, 0.3f);
            _progressbarView.Text.text = $"{_expCount}/100";
        }
        
        public int GetExp()
        {
            return _expCount;
        }

        public void ResetExp()
        {
            _expCount = 0;
            OnExpChanged?.Invoke(_expCount);
        }

        public void Init()
        {
            _playerExpObservable.OnGiveExp += AddExp;
        }

        public void Cleanup()
        {
            _playerExpObservable.OnGiveExp -= AddExp;
        }
    }
}