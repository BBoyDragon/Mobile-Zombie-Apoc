﻿using System;
using Runtime.Controller;
using UI;

namespace Player
{
    public interface IPlayerExpControl: IInitialization, ICleanup
    {
        event Action<int> OnExpChanged;
        void AddExp(int count);
        int GetExp();
        void ResetExp();
        void SetProgressbar(ProgressbarView progressbar);
    }
}