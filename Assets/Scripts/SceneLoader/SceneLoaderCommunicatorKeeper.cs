﻿namespace Assets.Scripts.SceneLoader
{
    static class SceneLoaderCommunicatorKeeper
    {
        static SceneLoaderCommunicatorKeeper()
        {
            _sceneCommunicator = new SceneLoaderContext();
        }

        public static ISceneLoaderContext GetContext()
        {
            return _sceneCommunicator;
        }

        private static ISceneLoaderContext _sceneCommunicator;
    }
}
