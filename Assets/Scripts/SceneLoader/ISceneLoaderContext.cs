﻿using UnityEngine;

namespace Assets.Scripts.SceneLoader
{
    public interface ISceneLoaderContext
    {
        int SceneIndex { get; set; }
        int NextScene { get; set; }

        Sprite WallPaper { get; set; }
        public bool IsContextinitialized{ get; set; }
    }
}
