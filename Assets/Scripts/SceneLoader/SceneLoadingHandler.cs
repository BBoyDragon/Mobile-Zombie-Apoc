﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace Assets.Scripts.SceneLoader
{
    public class SceneLoadingHandler
    {
        public int SceneIndex => _sceneContext.SceneIndex;
        public Sprite WallPaper => _sceneContext.WallPaper;

        public SceneLoadingHandler(SceneLoadData scenLoadingData, 
            ISceneLoaderContext context)
        {
            _scenesData = scenLoadingData;
            _sceneContext = context;

            if (!_sceneContext.IsContextinitialized)
            {
                int sceneElementNumber = _scenesData.FirstSceneElementNumber;
                _sceneContext.SceneIndex = _scenesData[sceneElementNumber].SceneIndex;
                _sceneContext.NextScene = _scenesData[sceneElementNumber].NextSceneIndex;
                _sceneContext.WallPaper = _scenesData[sceneElementNumber].WallPaperImage;
                _sceneContext.IsContextinitialized = true;
            }
        }

        private void SwitchContextToNextScene()
        {
            int sceneIndex = _sceneContext.SceneIndex;
            int scenePositionInList = FindScenePosition(sceneIndex);
            if (scenePositionInList < 0)
            {
                scenePositionInList = _scenesData.FirstSceneElementNumber;
                sceneIndex = _scenesData[scenePositionInList].SceneIndex;
            }
            else
            {
                sceneIndex = _sceneContext.NextScene;
                scenePositionInList = FindScenePosition(sceneIndex);
            }

            _sceneContext.SceneIndex = sceneIndex;

            _sceneContext.NextScene = 
                _scenesData[scenePositionInList].NextSceneIndex;

            _sceneContext.WallPaper = 
                _scenesData[scenePositionInList].WallPaperImage;
        }

        private void SwitchContextToMenuScene()
        {
            if (0 > _scenesData.MenuSceneElementNumber 
                || _scenesData.MenuSceneElementNumber > _scenesData.Count)
            {
                Debug.LogError("0 > _scenesData.MenuSceneElementNumber " +
                "|| _scenesData.MenuSceneElementNumber > _scenesData.Count");
                return;
            }

            int menuSceneElementNumber = _scenesData.MenuSceneElementNumber;
            _sceneContext.NextScene = _sceneContext.SceneIndex;

            _sceneContext.SceneIndex = 
                _scenesData[menuSceneElementNumber].SceneIndex;

            _sceneContext.WallPaper = 
                _scenesData[menuSceneElementNumber].WallPaperImage;
        }

        public void LoadNextScene() 
        {
            SwitchContextToNextScene();
            SceneManager.LoadScene(
                _scenesData.SceneLoaderIndex, LoadSceneMode.Single);
        }

        public void LoadMenu()
        {
            SwitchContextToMenuScene();

            SceneManager.LoadScene(
                _scenesData.SceneLoaderIndex, LoadSceneMode.Single);
        }

        private int FindScenePosition(int sceneIndex)
        {
            if (sceneIndex < 0)
                return -1;

            for (int i = 0; i < _scenesData.Count; ++ i)
            {
                if (_scenesData[i].SceneIndex == sceneIndex)
                    return i;
            }
            return -1;
        }

        private SceneLoadData _scenesData;
        private ISceneLoaderContext _sceneContext;
    }
}
