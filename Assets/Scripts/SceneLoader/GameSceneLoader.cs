using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.Scripts.SceneLoader;
using DG.Tweening;

public class GameSceneLoader : MonoBehaviour
{
    [SerializeField] private Image _progressBar;
    [SerializeField] private Text _loadingText;
    [SerializeField] private Image _wall;
    [SerializeField] private SceneLoadData _scenesData;

    void Start()
    {
        _currentProgress = 0;
        _targetProgress = 0;
        _sceneLoadingHandler = new SceneLoadingHandler(_scenesData, 
            SceneLoaderCommunicatorKeeper.GetContext());

        _progressBar.type = Image.Type.Filled;

        _progressBar.fillMethod = Image.FillMethod.Horizontal;
        _progressBar.fillAmount = 0f;

        StartCoroutine(AsyncLoadingScene());
    }

    IEnumerator AsyncLoadingScene()
    {
        int sceneIndex = _sceneLoadingHandler.SceneIndex;
        _wall.sprite = _sceneLoadingHandler.WallPaper;
        
        AsyncOperation operation = 
            SceneManager.LoadSceneAsync(sceneIndex, LoadSceneMode.Single);

        operation.allowSceneActivation = false;
        while (operation.progress < 0.9f)
        {
            _targetProgress = (int)(operation.progress * 100);

            _progressBar.fillAmount = operation.progress;

            yield return LoadProgress();
        }
        _targetProgress = 100;
        yield return LoadProgress();
        operation.allowSceneActivation = true;
    }

    private IEnumerator<WaitForEndOfFrame> LoadProgress()
    {
        while (_currentProgress < _targetProgress)
        {
            ++_currentProgress;
            _loadingText.text = string.Format("{0}%", 
                _currentProgress.ToString());

            _progressBar.fillAmount = (float)_currentProgress / 100.0f;

            yield return new WaitForEndOfFrame();
        }
    }


    private SceneLoadingHandler _sceneLoadingHandler;

    private int _currentProgress;
    private int _targetProgress;
}
