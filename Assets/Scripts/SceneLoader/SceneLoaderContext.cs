﻿using UnityEngine;

namespace Assets.Scripts.SceneLoader
{
    public class SceneLoaderContext : ISceneLoaderContext
    {
        public SceneLoaderContext()
        {
            IsContextinitialized = false;
        }

        public int SceneIndex 
        {
            get;
            set;
        }

        public int NextScene 
        { 
            get; 
            set; 
        }

        public Sprite WallPaper 
        { 
            get; 
            set; 
        }

        public bool IsContextinitialized 
        { 
            get; 
            set; 
        }
    }
}
