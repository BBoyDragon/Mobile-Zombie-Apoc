﻿using System;
using UnityEngine;

namespace Assets.Scripts.Data
{
    [Serializable]
    public class SceneDescriptor
    {
        public int SceneIndex;
        public int NextSceneIndex;

        public Sprite WallPaperImage;
    }
}
