﻿using UI;
using UnityEngine;
using WeaponMelee;

namespace Data
{
    [CreateAssetMenu(fileName = "PlayerData", menuName = "Data/PlayerData", order = 0)]
    public class PlayerData : ScriptableObject
    {
        [SerializeField] private GameObject prefab;
        [SerializeField] private ProgressbarView healthBarView;
        [SerializeField] private ProgressbarView reloadBarView;
        [SerializeField] private Weapon.Weapon _weapon;
        [SerializeField] private WeaponMelee.WeaponMelee weaponMelee;

        [Header("PlayerStats")]
        [Tooltip("Скорость бега")]
        [SerializeField] private float speed;
        [Tooltip("Здоровье")]
        [SerializeField] private int health;
        [Tooltip("Меткость - % на который уменьшается разброс оружия"), Range(0, 1)]
        [SerializeField] private float accuracy;
        [Tooltip("Шанс крита"), Range(0, 1)]
        [SerializeField] private float critChance;
        [Tooltip("Сила крита")]
        [SerializeField] private float critPower;
        [Tooltip("Боезапас (максимальное количество патронов)")]
        [SerializeField] private int ammunition;
        [Tooltip("% доп опыта"), Range(0,1)]
        [SerializeField] private float additionalExperience;
        [Tooltip("Скорость перезарядки (% на который уменьшается перезарядка оружия)")]
        [SerializeField] private float reloadSpeed;
        [Tooltip("Увеличение получение отхила"), Range(0,1)]
        [SerializeField] private float healingGain;
        [Tooltip("Уворот - шанс не получить урон от атаки"), Range(0,1)]
        [SerializeField] private float dodgeChance;
        [Tooltip("Регенерация здоровья"), Range(0,1)]
        [SerializeField] private float healthRegeneration;
        [Tooltip("Броня - величина, на которую уменьшается получаемый урон")]
        [SerializeField] private int armor;
        [Tooltip("Ускорение захвата цели (% на который уменьшается аналогичный параметр оружия)"), Range(0,1)]
        [SerializeField] private float targetCaptureSpeed;
        [SerializeField, Tooltip("Сколько нужно опыта для получения одной монеты")]
        private int expNeedForOneCoin;

        public Weapon.Weapon Weapon => _weapon;
        
        public GameObject Prefab => prefab;
        public float CritPower => critPower;
        public int Armor => armor;
        public float CritChance => critChance;
        public float TargetCaptureSpeed => targetCaptureSpeed;
        public float ReloadSpeed => reloadSpeed;
        public float Accuracy => accuracy;
        public float DodgeChance => dodgeChance;
        public float HealingGain => healingGain;
        public int Ammunition => ammunition;
        public float AdditionalExperience => additionalExperience;
        public float HealthRegeneration => healthRegeneration;

        public float Speed => speed;

        public ProgressbarView HealthBarView => healthBarView;
        public ProgressbarView ReloadBarView => reloadBarView;
        public WeaponMelee.WeaponMelee WeaponMelee => weaponMelee;

        public int Health => health;

        public int ExpNeedForOneCoin => expNeedForOneCoin;
    }
}