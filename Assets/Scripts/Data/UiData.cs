﻿using System;
using Player;
using TMPro;
using UI;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "UIData", menuName = "Data/UIData", order = 0)]
    public class UiData : ScriptableObject
    {
        [SerializeField] private PlayerControlView _playerControl;

        [SerializeField] private AttackMeleeView _attackMeleePrefab;
        [SerializeField] private Canvas uiCanvas;
        [SerializeField] private ProgressbarView expProgressbarPrefab;
        [SerializeField] private TMP_Text amoCountText;
        public PlayerControlView PlayerControlView => _playerControl;

        public Canvas UICanvas => uiCanvas;

        public AttackMeleeView AttackMeleePrefab => _attackMeleePrefab;

        public ProgressbarView ExpProgressbarPrefab => expProgressbarPrefab;

        public TMP_Text AmoCountText => amoCountText;

    }
}