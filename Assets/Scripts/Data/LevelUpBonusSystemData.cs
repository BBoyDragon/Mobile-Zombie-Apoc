﻿using LevelUpBonusSystem;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "LevelUpBonusSystemData", menuName = "Data/LevelUpBonusSystemData", order = 0)]
    public class LevelUpBonusSystemData : ScriptableObject
    {
        [SerializeField] 
        private LevelUpBonusManagerView levelUpBonusManagerView;
        [SerializeField]
        private BonusEffect[] bonusEffects;

        public LevelUpBonusManagerView LevelUpBonusManagerView => levelUpBonusManagerView;
        public BonusEffect[] BonusEffects => bonusEffects;
    }
}
