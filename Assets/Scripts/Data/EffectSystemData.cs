﻿using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "EffectSystemData", menuName = "Data/EffectSystemData", order = 0)]
    public class EffectSystemData : ScriptableObject
    {
        [SerializeField]
        private GameObject levelUpEffect;

        public GameObject LevelUpEffect => levelUpEffect;
    }
}
