﻿using UnityEngine;

namespace Data.Currencies
{
    [CreateAssetMenu(fileName = "CurrencyData", menuName = "Data/Currencies/CurrencyData", order = 0)]
    public class CurrencyData: ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField] private Sprite _icon;

        public string Name => _name;
        public Sprite Icon => _icon;
    }
}