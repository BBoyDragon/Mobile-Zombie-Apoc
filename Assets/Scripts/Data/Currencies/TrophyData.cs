﻿using UnityEngine;

namespace Data.Currencies
{
    [CreateAssetMenu(fileName = "TrophyData", menuName = "Data/Currencies/TrophyData", order = 0)]
    public class TrophyData: ScriptableObject
    {
        [SerializeField] private string _name = "Трофей";
        [SerializeField] private Sprite _icon;
        [SerializeField, Range(1, 100), Tooltip("Шанс выпадения трофея")] 
        private int _dropChance = 2;
        
        public string Name => _name;
        public Sprite Icon => _icon;
        public int DropChance => _dropChance;
    }
}