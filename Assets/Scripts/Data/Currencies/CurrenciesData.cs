﻿using System.Collections.Generic;
using Economy;
using UnityEngine;

namespace Data.Currencies
{
    [CreateAssetMenu(fileName = "CurrenciesData", menuName = "Data/CurrenciesData", order = 0)]
    public class CurrenciesData: ScriptableObject
    {
        [SerializeField] private List<TrophyData> _trophies;
        [SerializeField] private List<CurrencyData> _currencies;
        
        [SerializeField, Tooltip("Сколько нужно опыта для получения одной монеты")] 
        private int _expNeedForOneCurrencyCoin;
        
        [SerializeField, Tooltip("Валюта в которую будет переводится опыт")] 
        private CurrencyData _expToCurrencyData;

        public List<TrophyData> Trophies => _trophies;
        public List<CurrencyData> Currencies => _currencies;
        public int ExpNeedForOneCoin => _expNeedForOneCurrencyCoin;
        public CurrencyData ExpToCurrencyData => _expToCurrencyData;
    }
}