﻿using System.IO;
using Data.Currencies;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "Data", menuName = "Data/Data")]
    public class Data : ScriptableObject
    {
        [SerializeField] private string mapDataPath;
        private MapData _mapData;
        [SerializeField] private string playerDataPath;
        private PlayerData _playerData;
        [SerializeField] private string uiDataPath;
        private UiData _uiData;
        [SerializeField] private string cameraDataPath;
        private CameraData _cameraData;
        [SerializeField] private string enemiesDataPath;
        private EnemiesData _enemiesData;
        [SerializeField] private string musicDataPath;
        private MusicData _musicData;

        [SerializeField] private string levelUpBonusSystemDataPath;
        private LevelUpBonusSystemData _levelUpBonusSystemData;
        [SerializeField] private string effectSystemDataPath;
        private EffectSystemData _effectSystemData;
        [SerializeField] private string weaponLevelingSystemDataPath;
        private WeaponLevelingSystemData _weaponLevelingSystemData;

        [SerializeField] private string currencyDataPath;
        private CurrenciesData currenciesData;
        [SerializeField] private string fogOfWarDataPath;
        private FogOfWarData _fogOfWarData;



        [SerializeField] private string pathToLevel = "Data/";

      
        public MusicData MusicData
        {
            get
            {
                if (_musicData == null)
                {
                    _musicData = Load<MusicData>(pathToLevel + musicDataPath);
                }

                return _musicData;
            }
        }
        public EnemiesData EnemiesData
        {
            get
            {
                if (_enemiesData == null)
                {
                    _enemiesData = Load<EnemiesData>(pathToLevel + enemiesDataPath);
                }

                return _enemiesData;
            }
        }

        public CameraData CameraData
        {
            get
            {
                if (_cameraData == null)
                {
                    _cameraData = Load<CameraData>(pathToLevel + cameraDataPath);
                }

                return _cameraData;
            }
        }

        public UiData UiData
        {
            get
            {
                if (_uiData == null)
                {
                    _uiData = Load<UiData>(pathToLevel + uiDataPath);
                }

                return _uiData;
            }
        }

        public MapData MapData
        {
            get
            {
                if (_mapData == null)
                {
                    _mapData = Load<MapData>(pathToLevel + mapDataPath);
                }

                return _mapData;
            }
        }

        public PlayerData PlayerData
        {
            get
            {
                if (_playerData == null)
                {
                    _playerData = Load<PlayerData>(pathToLevel + playerDataPath);
                }

                return _playerData;
            }
        }
        
        public CurrenciesData CurrenciesData
        {
            get
            {
                if (currenciesData == null)
                {
                    currenciesData = Load<CurrenciesData>(pathToLevel + currencyDataPath);
                }

                return currenciesData;
            }
        }


        public FogOfWarData FogOfWarData
        {
            get
            {
                if (_fogOfWarData == null)
                {
                    _fogOfWarData = Load<FogOfWarData>(pathToLevel + fogOfWarDataPath);
                }

                return _fogOfWarData;
            }
        }

        public LevelUpBonusSystemData LevelUpBonusSystemData
        {
            get
            {
                if (_levelUpBonusSystemData == null)
                {
                    _levelUpBonusSystemData = Load<LevelUpBonusSystemData>(pathToLevel + levelUpBonusSystemDataPath);
                }

                return _levelUpBonusSystemData;
            }
        }

        public EffectSystemData EffectSystemData
        {
            get
            {
                if (_effectSystemData == null)
                {
                    _effectSystemData = Load<EffectSystemData>(pathToLevel + effectSystemDataPath);
                }

                return _effectSystemData;
            }
        }

        public WeaponLevelingSystemData WeaponLevelingSystemData
        {
            get
            {
                if (_weaponLevelingSystemData == null)
                {
                    _weaponLevelingSystemData = Load<WeaponLevelingSystemData>(pathToLevel + weaponLevelingSystemDataPath);
                }

                return _weaponLevelingSystemData;
            }
        }

        private static T Load<T>(string resourcesPath) where T : Object =>
            Resources.Load<T>(Path.ChangeExtension(resourcesPath, null));
    }
}