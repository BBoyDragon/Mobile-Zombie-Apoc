using Assets.Scripts.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SceneLoadData", menuName = "Data/SceneLoadData")]
public class SceneLoadData : ScriptableObject, IEnumerable<SceneDescriptor>
{
    [SerializeField] private int _sceneLoaderIndex;
    [SerializeField] private int _menuSceneElementNumber;
    [SerializeField] private int _firstSceneElementNumber;

    [SerializeField] private List<SceneDescriptor> _scenesList;

    public int SceneLoaderIndex => _sceneLoaderIndex;
    public int MenuSceneElementNumber => _menuSceneElementNumber;
    public int FirstSceneElementNumber => _firstSceneElementNumber;

    public IEnumerator<SceneDescriptor> GetEnumerator()
    {
        return _scenesList.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return _scenesList.GetEnumerator();
    }

    public SceneDescriptor this[int index]
    {
        get => _scenesList[index];
        set => _scenesList[index] = value;
    }

    public int Count 
    { 
        get => _scenesList.Count;
    }
}


