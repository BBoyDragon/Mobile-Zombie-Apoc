﻿using System;
using System.Collections.Generic;
using Data.Currencies;
using DefaultNamespace;
using Dialoge_Editor.Runtime;
using Economy;
using Enemy;
using Player;
using UI;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "EnemiesData", menuName = "Data/EnemiesData", order = 0)]
    public class EnemiesData : ScriptableObject
    {
        [SerializeField] private List<EnemyInfo> enemies;
        [SerializeField] private float timeFromSpawn;
        [SerializeField] private float timeToSpawn;

        public List<EnemyInfo> Enemies => enemies;
        public (float from, float to) TimeSpawn => (timeFromSpawn, timeToSpawn);
    }

    [Serializable]
    public struct EnemyInfo
    {
        [SerializeField] private string name;
        [SerializeField] private GameObject enemyPrefab;
        [SerializeField] private float speed;
        [SerializeField] private float attackSpeed;
        [SerializeField] private AIContainer container;
        [SerializeField] private int expCountOnDeath;
        [SerializeField] private float health;
        [SerializeField] private ProgressbarView aimTimerView;
        [SerializeField] private ProgressbarView healthBarView;
        [SerializeField] private int dmg;
        [SerializeField] private float attackCoooldown;
        [SerializeField] private TrophyData _trophy;
        
        public ProgressbarView AimTimerView => aimTimerView;

        public ProgressbarView HealthBarView => healthBarView;

        public float AttackCoooldown => attackCoooldown;

        public int Dmg => dmg;

        public AIContainer Container => container;

        public string Name => name;

        public TrophyData Trophy => _trophy;
        public GameObject EnemyPrefab => enemyPrefab;
        public float Speed => speed;

        public float AttackSpeed => attackSpeed;
        public int ExpCountOnDeath => expCountOnDeath;

        public float Health => health; 
       
    }
}