﻿using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "FogOfWarData", menuName = "Data/FogOfWarData", order = 0)]
    public class FogOfWarData : ScriptableObject
    {
        [Header("Настройка зоны видимости игрока")]
        [SerializeField, Tooltip("Включение/Выключение пропадания противников которые находятся в тумане")] 
        private bool _viewAreaEnabled;
        [SerializeField] private GameObject _viewAreaPrefab;
        
        [Header("Настройка тумана")]
        [SerializeField] private bool _fogEnabled;
        [SerializeField] private GameObject _fogPrefab;
        [SerializeField] private Color _fogColor;

        [SerializeField, Range(0.1f, 10f)] private float _fogViewAreaRadius = 8f;
        [SerializeField, Range(0.1f, 1f)] private float _fogViewAreaBorderSize = 0.5f;

        [Header("Детализация Тумана")]
        [SerializeField, Range(1, 254), Tooltip("Рекомендуется ставить значение как у размера тумана и выше")] 
        private int _fogSegmentsWidth = 150;
        [SerializeField, Range(1, 254), Tooltip("Рекомендуется ставить значение как у размера тумана и выше")] 
        private int _fogSegmentsLength = 150;
        
        [Header("Размер Тумана")]
        [SerializeField] private float _fogWidth = 150f;
        [SerializeField] private float _fogLength = 150f;

        public bool ViewAreaEnabled => _viewAreaEnabled;
        public GameObject ViewAreaPrefab => _viewAreaPrefab;
        
        public bool FogEnabled => _fogEnabled;
        public GameObject FogPrefab => _fogPrefab;
        public Color FogColor => _fogColor;
        
        public float FogViewAreaRadius => _fogViewAreaRadius;
        public float FogViewAreaBorderSize => _fogViewAreaBorderSize;
        
        public int FogSegmentsWidth => _fogSegmentsWidth;
        public int FogSegmentsLength => _fogSegmentsLength;
        
        public float FogWidth => _fogWidth;
        public float FogLength => _fogLength;
    }
}