﻿using UnityEngine;
using WeaponLevelingSystem;

namespace Data
{
    [CreateAssetMenu(fileName = "WeaponLevelingSystemData", menuName = "Data/WeaponLevelingSystemData", order = 0)]
    public class WeaponLevelingSystemData : ScriptableObject
    {
        [SerializeField]
        private Sprite toolSprite;
        [SerializeField]
        private WeaponLevelingToolView weaponLevelingToolView;
        [SerializeField]
        private WeaponLevelingManagerView weaponLevelingManagerView;
        [SerializeField]
        private WeaponLevelingEffect[] weaponLevelingEffect;

        public Sprite ToolSprite => toolSprite;
        public WeaponLevelingToolView WeaponLevelingToolView => weaponLevelingToolView;
        public WeaponLevelingManagerView WeaponLevelingManagerView => weaponLevelingManagerView;
        public WeaponLevelingEffect[] WeaponLevelingEffect => weaponLevelingEffect;
    }
}
