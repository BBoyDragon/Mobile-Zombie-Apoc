using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectPool<T>: IDisposable where T : class 
{
    private Func<T> _createFunction;
    private event Action<T> _onGetObjectFromPool;
    private event Action<T> _onDisableObject;
    private event Action<T> _onRemoveObject;
    private int _maxSize;
    private Stack<T> _pool;
    
    public ObjectPool(
        Func<T> createFunction, 
        Action<T> onGetObjectFromPool = null,
        Action<T> onDisableObject = null,
        Action<T> onRemoveObject = null, 
        int maxSize = 10,
        int defaultCapasity = 1000)
    {
        _createFunction = createFunction;
        _onGetObjectFromPool += onGetObjectFromPool;
        _onDisableObject += onDisableObject;
        _onRemoveObject += onRemoveObject;
        _maxSize = maxSize;
        
        _pool = new Stack<T>(defaultCapasity);
    }

    public T GetObject()
    {
        if (!_pool.Any())
        {
            var newObj = _createFunction.Invoke();
            _onGetObjectFromPool?.Invoke(newObj);
            return newObj;
        }
        if (_pool.Count() > _maxSize)
        {
            Debug.LogError($"Пул объектов типа {typeof(T)} переполнен!");
            return default(T);
        }

        var obj = _pool.Pop();
        _onGetObjectFromPool?.Invoke(obj);
        return obj;
        
    }

    public void ReleaseObject(T obj)
    {
        if(_pool.Contains(obj))
        {
            Debug.LogError($"Объект уже находися в пуле объектов типа {typeof(T)}");
            return;
        }
        _onDisableObject?.Invoke(obj);
        _pool.Push(obj);
    }

    public void Clear()
    {
        while (_pool.Any())
        {
            _onRemoveObject?.Invoke(_pool.Pop());
        }
    }
    public void Dispose() => Clear();
    
}