﻿using DefaultNamespace;
using UnityEngine;

namespace Weapon
{
    [CreateAssetMenu(fileName = "Weapon", menuName = "Weapon/Weapon", order = 0)]
    public class Weapon : ScriptableObject
    {
        [SerializeField] private int dmg;
        [SerializeField] private WeaponView _weaponView;
        [SerializeField] private float bulletSpeed;
        [SerializeField] private float aimTime;
        [SerializeField] private float shotFrequency;
        [SerializeField] private float aimDistance;
        [SerializeField] private int throughCount;
        [SerializeField, Range(0, 1)] private float misfireChance;
        [SerializeField] private int clipCount;
        [SerializeField] private int amoCount;
        [SerializeField] private int reloadTime;


        public int Dmg => dmg;
        public WeaponView WeaponView => _weaponView;

        public float BulletSpeed => bulletSpeed;

        public float AimTime => aimTime;

        public float AimDistance => aimDistance;

        public float ShotFrequency => shotFrequency;

        public int ThroughCount => throughCount;

        public float MisfireChance => misfireChance;

        public int ClipCount => clipCount;

        public int AmoCount => amoCount;

        public int ReloadTime => reloadTime;
    }
}