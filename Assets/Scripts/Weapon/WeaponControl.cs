using System;
using DG.Tweening;
using Enemy;
using Singleton;
using UI;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Weapon
{
    public class WeaponControl
    {
        private Weapon _weapon;

        private bool _isReloading;
        private bool _canShoot;
        private int _currentAmoCount;
        private int _currentClipCount;
        private ObjectPool<WeaponView> _bulletPool;
        private ProgressbarView _reloadProgerssView;

        public event Action<int, int> OnAmoCountChanged;

        private Transform _shootingStart;

        public IAimable Target { get; private set; }

        public WeaponControl(Weapon weapon, ProgressbarView progressbarView)
        {
            _weapon = weapon;
            _reloadProgerssView = progressbarView;
            _currentClipCount = _weapon.ClipCount;
            _currentAmoCount = weapon.AmoCount;
            _isReloading = false;
            _canShoot = true;
            _reloadProgerssView.gameObject.SetActive(false);
            var bulletParrant = new GameObject("Bullets");

            _bulletPool = new ObjectPool<WeaponView>(
                () => Object.Instantiate(weapon.WeaponView, bulletParrant.transform)
                    .SetUpWeapon(weapon, view => _bulletPool?.ReleaseObject(view)),
                view => view.gameObject.SetActive(true),
                view => view.gameObject.SetActive(false),
                view => Object.Destroy(view.gameObject),
                50, 20);
        }

        public bool LockTarget(Transform playerView)
        {
            Debug.DrawRay(playerView.position + Vector3.down * 0.2f,
                playerView.forward * _weapon.AimDistance, Color.red);
            _shootingStart = playerView;
            if (!Physics.Raycast(
                    playerView.transform.position + Vector3.down * 0.2f, playerView.forward,
                    out var hit, _weapon.AimDistance))
            {
                if (Target is null) return false;
                Target.OnShootReady -= Shoot;
                Target.HideUI();
                Target = null;
                return false;
            }

            var aimable = hit.collider.gameObject.GetComponent<IAimable>();

            if (aimable is null || !aimable.CanAim())
            {
                if (Target is null) return false;
                Target.OnShootReady -= Shoot;
                Target.HideUI();
                Target = null;
                return false;
            }

            if (Target != null && Target != aimable &&
                Vector3.Distance(Target.GetTarget().position, aimable.GetTarget().position) <= 20)
            {
                Target.OnShootReady -= Shoot;
                Target.HideUI();
                Target = aimable;
                Aim();
                Target.LockTarget();
            }
            else if (Target is null)
            {
                Target = aimable;
                Aim();
            }


            return true;
        }

        private void Aim()
        {
            Target.ShowUI(_weapon.AimTime);
            Target.OnShootReady += Shoot;
        }


        private void Shoot()
        {
            if (_isReloading || !_canShoot || Target is null || _currentAmoCount <= 0 || IsMissFire()) return;


            var view = _bulletPool.GetObject();
            view.transform.SetPositionAndRotation(
                _shootingStart.position + _shootingStart.forward * 1.5f, Quaternion.identity);
            view.Shoot(Target.GetTarget().position);
            _canShoot = false;
            TimerHelper.Instance.StartTimer(() =>
            {
                _canShoot = true;
                Shoot();
            }, _weapon.ShotFrequency);
            _currentClipCount--;
            OnAmoCountChanged?.Invoke(_currentClipCount, _currentAmoCount);
            if (_currentClipCount == 0)
            {
                Reload();
            }
        }

        private bool IsMissFire()
        {
            if (Random.Range(0f, 1f) > _weapon.MisfireChance) return false;
            Debug.Log("Осечка!!");
            Reload(true);
            return true;
        }

        private void Reload(bool isMissfire = false)
        {
            var time = isMissfire ? 0.5f : _weapon.ReloadTime;
            _reloadProgerssView.ProgressBar.fillAmount = 0;
            _reloadProgerssView.gameObject.SetActive(true);
            _isReloading = true;
            Debug.Log("Reloading");
            _reloadProgerssView.ProgressBar.DOFillAmount(1f, time).OnComplete((() =>
            {
                _isReloading = false;
                _reloadProgerssView.gameObject.SetActive(false);
                Debug.Log("Reloaded");
                if (Target != null)
                    Shoot();
                if (isMissfire) return;
                _currentClipCount = _weapon.ClipCount;
                _currentAmoCount -= _currentClipCount;
                OnAmoCountChanged?.Invoke(_currentAmoCount, _currentClipCount);
            }));
        }
    }
}