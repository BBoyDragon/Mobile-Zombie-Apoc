using System;
using DG.Tweening;
using Enemy;
using UnityEngine;

namespace Weapon
{
    [RequireComponent(typeof(Collider))]
    public class WeaponView : MonoBehaviour
    {
        private Weapon _weapon;
        private int throughCount = 0;
        private event Action<WeaponView> OnDisable;

        public void Shoot(Vector3 target)
        {
            
            transform.DOMove(target, _weapon.BulletSpeed).SetEase(Ease.Linear).OnComplete(() => OnDisable?.Invoke(this));
            transform.LookAt(target);
        }

        public WeaponView SetUpWeapon(Weapon weapon, Action<WeaponView> onDisable)
        {
            _weapon = weapon;
            OnDisable += onDisable;
            return this;
        }

        private void OnCollisionEnter(Collision collision)
        {
            var damagable = collision.gameObject.GetComponent<IDamagable>();

            damagable?.Hit(_weapon.Dmg);

            if (throughCount++ <= _weapon.ThroughCount) return;

            transform.DOKill();
            OnDisable?.Invoke(this);
        }
    }
}