﻿using UnityEngine;

namespace WeaponLevelingSystem
{
    public abstract class WeaponLevelingEffect : ScriptableObject
    {
        [SerializeField]
        public Sprite Sprite;
        public abstract void Apply(Player.Player target);
        public abstract string Text { get; }
    }
}
