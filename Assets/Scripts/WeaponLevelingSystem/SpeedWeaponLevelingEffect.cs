﻿using UnityEngine;

namespace WeaponLevelingSystem
{
    [CreateAssetMenu(fileName = "SpeedWeaponLevelingEffect", menuName = "WeaponLevelingEffect/SpeedWeaponLevelingEffect", order = 0)]
    public class SpeedWeaponLevelingEffect : WeaponLevelingEffect
    {
        [SerializeField]
        private float _extraSpeed;
        [SerializeField]
        private string _description;
        public override string Text => $"{_description} {_extraSpeed}";
        public override void Apply(Player.Player target)
        {
            Debug.Log("Бонус скорости оружия применен");
            //target.CurrentWeapon.ApplyLevelingEffect(_extraSpeed);
        }
    }
}
