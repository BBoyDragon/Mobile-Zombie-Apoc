﻿using Data;
using Runtime.Controller;
using UnityEngine;

namespace WeaponLevelingSystem
{
    public class WeaponLevelingSystemController : IInitialization
    {
        private WeaponLevelingToolView _instanseWeaponLevelingTool { get; }
        private WeaponLevelingManagerView _weaponLevelingManagerView { get; }
        private WeaponLevelingSystemData  _weaponLevelingSystemData { get; }

        private WeaponLevelingManagerView _instanse;

        private Transform _parentCanvas;

        private int _levelCount;

        private Player.Player _player;

        public void Init()
        {

        }

        public WeaponLevelingSystemController(Player.Player player, WeaponLevelingSystemData weaponLevelingSystemData, Canvas UICanvas)
        {
            _player = player;
            player.OnLevelUp += LevelUpAction;
            _weaponLevelingSystemData = weaponLevelingSystemData;
            _weaponLevelingManagerView = weaponLevelingSystemData.WeaponLevelingManagerView;
            _parentCanvas = UICanvas.transform;

            weaponLevelingSystemData.WeaponLevelingToolView.Init(weaponLevelingSystemData.ToolSprite);
            _instanseWeaponLevelingTool = GameObject.Instantiate(weaponLevelingSystemData.WeaponLevelingToolView, _parentCanvas);
            _instanseWeaponLevelingTool.OnNeedChooseTool += ActivateWeaponLevelingSystem;
        }

        private void LevelUpAction(Player.Player player)
        {
            _levelCount++;

            if (_levelCount % 2 != 0)
                return;

            _instanseWeaponLevelingTool.IncreaseCountTool();

        }

        private void ActivateWeaponLevelingSystem()
        {
            _instanse = GameObject.Instantiate(_weaponLevelingManagerView, _parentCanvas);
            _instanse.Init(_player);
            _instanse.OnWorkIsDone += DestroyInstanse;
            _instanse.SetBonusCard(_weaponLevelingSystemData.WeaponLevelingEffect);
        }

        private void DestroyInstanse()
        {
            _instanse.OnWorkIsDone -= DestroyInstanse;
            Object.Destroy(_instanse.gameObject);
        }
    }
}
