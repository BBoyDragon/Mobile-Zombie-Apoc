﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace WeaponLevelingSystem
{
    public class WeaponLevelingToolView : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField]
        private Image _toolImage;
        [SerializeField]
        private TMP_Text _textCount;
        private int _countTool;

        public System.Action OnNeedChooseTool;

        public void Init(Sprite sprite)
        {
            _toolImage.sprite = sprite;
            Refresh();
        }
        public void OnPointerDown(PointerEventData eventData)
        {
            Debug.Log(this.gameObject.name + " Was Clicked.");
            
            if (_countTool > 0)
            {
                _countTool--;
                Refresh();
                OnNeedChooseTool?.Invoke();
                Debug.Log($"Выбран инструмент прокачки оружия");
            }
            else
                Debug.Log($"Выбран инструмент прокачки оружия, но количество 0. Ничего не покажу.");
        }

        public void IncreaseCountTool()
        {
            _countTool++;
            Refresh();
        }

        private void Refresh()
        {
            UpdateText();
            UpdateVisible();
        }
        private void UpdateText()
        {
            _textCount.text = $"{_countTool}";
        }

        private void UpdateVisible()
        {
            if (_countTool > 0)
                gameObject.SetActive(true);
            else
                gameObject.SetActive(false);
        }

    }
}
