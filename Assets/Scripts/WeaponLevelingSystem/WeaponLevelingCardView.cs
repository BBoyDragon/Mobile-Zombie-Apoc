﻿using Singleton;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace WeaponLevelingSystem
{
    public class WeaponLevelingCardView : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField]
        private WeaponLevelingEffect _weaponLevelingEffect;
        [SerializeField]
        private Image _panelImage;
        [SerializeField]
        private Color _activeColor;
        [SerializeField]
        private Image _image;
        [SerializeField]
        private TMP_Text _text;

        public System.Action<WeaponLevelingEffect> OnClick;

        public void SetBonusEffect(WeaponLevelingEffect weaponLevelingEffect)
        {
            _weaponLevelingEffect = weaponLevelingEffect;
            _text.text = _weaponLevelingEffect.Text;
            _image.sprite = _weaponLevelingEffect.Sprite;
        }
        public void OnPointerDown(PointerEventData eventData)
        {
            Debug.Log(this.gameObject.name + " Was Clicked.");
            Debug.Log($"Выбрана прокачка оружия {_text.text}");
            _panelImage.color = _activeColor;
            _text.color = Color.black;
            TimerHelper.Instance.StartTimer(InvokeEvent, 0.5f);
        }

        private void InvokeEvent()
        {
            OnClick?.Invoke(_weaponLevelingEffect);
        }

    }
}
