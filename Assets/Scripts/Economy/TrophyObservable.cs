﻿using System;
using System.Collections.Generic;
using Enemy;
using Runtime.Controller;

namespace Economy
{
    public class TrophyObservable: ICleanup
    {
        public event Action<int> OnGiveTrophy;

        private readonly List<ITrophyGiver> _callers;

        public TrophyObservable()
        {
            _callers = new List<ITrophyGiver>();
        }
    
        public void AddСaller(ITrophyGiver trophyGiver)
        {
            trophyGiver.OnGiveTrophy += GiveTrophy;
            _callers.Add(trophyGiver);
        }

        public void RemoveСaller(ITrophyGiver trophyGiver)
        {
            trophyGiver.OnGiveTrophy -= GiveTrophy;
            _callers.Remove(trophyGiver);
        }

        private void GiveTrophy(ITrophyGiver trophyGiver, int trophyId)
        {
            RemoveСaller(trophyGiver);
            OnGiveTrophy?.Invoke(trophyId);
        }

        public void Cleanup()
        {
            foreach (var trophyGiver in _callers)
            {
                trophyGiver.OnGiveTrophy -= GiveTrophy;
            }
            _callers.Clear();
        }
    }
}