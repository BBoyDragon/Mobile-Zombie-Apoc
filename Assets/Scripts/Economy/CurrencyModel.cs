﻿using System;
using Data.Currencies;

namespace Economy
{
    public class CurrencyModel: ICurrencyModel
    {
        public event Action<int> OnCurrencyUpdated;

        private readonly int _id;
        private readonly CurrencyData _data;
        
        private int _count;

        public int Id => _id;
        public int Count => _count;
        public CurrencyData Data => _data;

        public CurrencyModel(CurrencyData currencyData)
        {
            _id = currencyData.GetInstanceID();
            _data = currencyData;
        }

        public void AddCurrency(int count)
        {
            _count += count;
            OnCurrencyUpdated?.Invoke(_count);
        }

        public void RemoveCurrency(int count)
        {
            _count -= count;
            OnCurrencyUpdated?.Invoke(_count);
        }
    }
}