﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data;
using Data.Currencies;
using Player;
using Runtime.Controller;
using UnityEngine;

namespace Economy
{
    public class CurrencyController: ICleanup
    {
        private const int GiveTrophyCount = 1;
        
        private readonly int _expNeedForOneMoney;
        private readonly IPlayerExpControl _playerExpControl;
        private readonly TrophyObservable _trophyObservable;
        private readonly CurrenciesData currenciesData;

        private readonly Dictionary<int, TrophyModel> _trophies;
        private readonly Dictionary<int, CurrencyModel> _currencies;

        public CurrencyController(TrophyObservable trophyObservable, IPlayerExpControl playerExpControl, CurrenciesData currenciesData)
        {
            _expNeedForOneMoney = currenciesData.ExpNeedForOneCoin;
            _trophyObservable = trophyObservable;
            _playerExpControl = playerExpControl;
            
            this.currenciesData = currenciesData;

            _trophies = new Dictionary<int, TrophyModel>();
            _currencies = new Dictionary<int, CurrencyModel>();
            
            _trophyObservable.OnGiveTrophy += OnGiveTrophy;
            
            SetupTrophies();
            SetupCurrencies();
        }

        private void SetupTrophies()
        {
            foreach (var trophyInfo in currenciesData.Trophies)
            {
                var trophyModel = new TrophyModel(trophyInfo);
                _trophies.Add(trophyInfo.GetInstanceID(), trophyModel);
            }
        }
        
        private void SetupCurrencies()
        {
            foreach (var currencyData in currenciesData.Currencies)
            {
                var currencyModel = new CurrencyModel(currencyData);
                _currencies.Add(currencyModel.Id, currencyModel);
            }
        }

        private void OnGiveTrophy(int trophyId)
        {
            var trophy = GetTrophy(trophyId);
            trophy.AddTrophyWithKillCount(GiveTrophyCount);
        }
        
        public TrophyModel GetTrophy(int trophyId)
        {
            return _trophies[trophyId];
        }
        
        public CurrencyModel GetCurrency(int currencyId)
        {
            return _currencies[currencyId];
        }
        
        /// <param name="clearExp">Нужно ли обнулить опыт, после перевода опыта в валюту?</param>
        public CurrencyModel PlayerExpToCurrency(bool clearExp)
        {
            var currencyModel = GetCurrency(currenciesData.ExpToCurrencyData.GetInstanceID());
            currencyModel.AddCurrency(_playerExpControl.GetExp() / _expNeedForOneMoney);

            if (clearExp)
            {
                _playerExpControl.ResetExp();
            }

            return currencyModel;
        }

        public void Cleanup()
        {
            _trophyObservable.OnGiveTrophy -= OnGiveTrophy;
        }
    }
}