﻿using System;

namespace Economy
{
    public interface ITrophyModel
    {
        public event Action<int> OnTrophyUpdated;
    }
}