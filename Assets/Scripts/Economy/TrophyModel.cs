﻿using System;
using Data.Currencies;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Economy
{
    public class TrophyModel: ITrophyModel
    {
        public event Action<int> OnTrophyUpdated;

        private readonly int _id;
        private readonly TrophyData _data;
        private readonly int _dropChance;
        
        private int _count;

        public int Id => _id;
        public int Count => _count;
        public TrophyData Data => _data;

        public TrophyModel(TrophyData trophyData)
        {
            _id = trophyData.GetInstanceID();
            _data = trophyData;
            _dropChance = trophyData.DropChance;
        }

        public void AddTrophyWithKillCount(int count)
        {
            var chance = Random.Range(1, 100);
            if (chance < _dropChance)
            {
#if UNITY_EDITOR
                Debug.Log($"Trophy '{_data.Name}' added to Player!");
#endif
                AddTrophy(count);
            }
        }

        public void AddTrophy(int count)
        {
            _count += count;
            OnTrophyUpdated?.Invoke(_count);
        }

        public void RemoveTrophy(int count)
        {
            _count -= count;
            OnTrophyUpdated?.Invoke(_count);
        }
    }
}