﻿using System;

namespace Economy
{
    public interface ICurrencyModel
    {
        public event Action<int> OnCurrencyUpdated;
    }
}