﻿using UnityEngine;

namespace WeaponMelee
{
    [CreateAssetMenu(fileName = "WeaponMelee", menuName = "Weapon/WeaponMelee", order = 1)]
    public class WeaponMelee : ScriptableObject
    {
        [Header("Базовый урон")]
        [SerializeField] private int _damage;
        [Header("Вьюшка")]
        [SerializeField] private WeaponMeleeView _weaponMeleeView;
        [Header("Префаб оружия ближнего боя")]
        [SerializeField] private GameObject _gameobject;
        [Header("Кулдаун (с)")]
        [SerializeField] private float _frequency = 2;
        [Header("Дальность атаки")]
        [SerializeField] private float _distance;
        [Header("Диапазон угла атаки")]
        [Range(0, 180)]
        [SerializeField] private int _damageAngle = 0;

        public int Damage => _damage;
        public float Frequency => _frequency;
        public float Distance => _distance;
        public int DamageAngle => _damageAngle;
        public WeaponMeleeView WeaponMeleeView => _weaponMeleeView;
        public GameObject Gameobject => _gameobject;
    }
}
