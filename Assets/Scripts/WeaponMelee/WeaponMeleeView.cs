﻿using Enemy;
using Singleton;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponMelee
{
    public class WeaponMeleeView : MonoBehaviour
    {
        private WeaponMelee _weaponMelee;

        [Header("Точка середины оружия")]
        [SerializeField] private Transform _weaponMiddlePointAttack;
        private bool _isAttackTime;
        private int _playerID;
        private HashSet<int> _instancesID_OneHitOnAttack = new HashSet<int>();

        public System.Action EnterAttack;
        public System.Action ExitAttack;

        public void SetPlayerID(int playerID)
        {
            _playerID = playerID;
        }
        public void SetUpWeapon(WeaponMelee weaponMelee)
        {
            _weaponMelee = weaponMelee;
            var boxCollider = gameObject.GetComponent<BoxCollider>();
            boxCollider.size = new Vector3(boxCollider.size.x, boxCollider.size.y, _weaponMelee.Distance);
            boxCollider.center = new Vector3(boxCollider.center.x, boxCollider.center.y, _weaponMelee.Distance / 2f);
        }
        public void Attack()
        {
            if (_isAttackTime)
                return;

            gameObject.SetActive(true);
            Debug.Log("AttackMelee starts");
            StartAttack();
            //animator.SetTrigger("AttackMelee");         
        }

        private void StartAttack()
        {
            EnterAttack?.Invoke();
            TimerHelper.Instance.StartTimer(StopAttack, _weaponMelee.Frequency);
            _isAttackTime = true;
        }

        private void StopAttack()
        {
            ExitAttack?.Invoke();
            _isAttackTime = false;
            gameObject.SetActive(false);
            _instancesID_OneHitOnAttack.Clear();
        }

        private void OnTriggerEnter(Collider other)
        {
            var damagable = other.gameObject.GetComponent<IDamagable>();

            if (damagable == null)
                return;

            int instancesID = gameObject.GetInstanceID();
            if (_instancesID_OneHitOnAttack.Contains(instancesID) || instancesID == _playerID)
                return;

            _instancesID_OneHitOnAttack.Add(instancesID);
            damagable.Hit(_weaponMelee.Damage);
            Debug.Log($"We hit (AttackMelee) {other.gameObject.name} {other.gameObject.GetInstanceID()}");
        }
    }
}
