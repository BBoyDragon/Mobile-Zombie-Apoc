﻿using AI;
using Enemy;
using LevelUpBonusSystem;
using WeaponLevelingSystem;
using Economy;
using FogOfWar;
using Player;
using Singleton;
using UI;
using UnityEngine;

namespace Controller
{
    internal sealed class GameInit
    {
        public GameInit(Controllers controllers, Data.Data data)
        {
            MusicController.Instance.SetMusicData(data.MusicData);

            EffectController.Instance.SetEffectSystemData(data.EffectSystemData);


            MapField.MapField map = new MapField.MapField(data.MapData);
            map.Spawn();

            UIController uiController = new UIController(data.UiData);
            uiController.Spawn();

            TrophyObservable trophyObservable = new TrophyObservable();
            controllers.Add(trophyObservable);

            PlayerExpObservable playerExpObservable = new PlayerExpObservable();
            controllers.Add(playerExpObservable);

            Player.Player player =
                new Player.Player(data.PlayerData, uiController.PlayerControlView, playerExpObservable);
            player.Spawn(map.MapView.PlayerSpawnPos, uiController.AttackMeleeView);
            player.PlayerExpControl.SetProgressbar(uiController.ExProgressbarView);
            controllers.Add(player.GetController());
            controllers.Add(player.GetExpController());

            CurrencyController сurrencyController =
                new CurrencyController(trophyObservable, player.PlayerExpControl, data.CurrenciesData);
            controllers.Add(сurrencyController);

            


            FogControl fogControl = new FogControl(data.FogOfWarData, player.Instance, Camera.main);
            fogControl.Spawn(player.Instance.transform, player.Instance.transform);
            controllers.Add(fogControl);


            uiController.AmoCountText.text = $"{data.PlayerData.Weapon.ClipCount}/{data.PlayerData.Weapon.AmoCount}";
            player.PlayerControl.WeaponControl.OnAmoCountChanged += (clipCount, amoCount) =>
            {
                uiController.AmoCountText.text = $"{clipCount}/{amoCount}";
            };


            CameraControl cameraControl = new CameraControl(Camera.main, data.CameraData);
            cameraControl.SetTarget(player.Instance);
            controllers.Add(cameraControl);

            EnemySpawnController spawnController =
                new EnemySpawnController(data.EnemiesData, map.MapView.EnemySpawnPoses, data.FogOfWarData);


            AIManager aiManager = new AIManager(player.Instance);

            controllers.Add(aiManager);


            spawnController.OnEnemySpawned += enemy =>
            {
                aiManager.Add(enemy.Agent);
                enemy.OnGiveExp += (_, count) => player.PlayerExpControl.AddExp(count);
                playerExpObservable.AddСaller(enemy);
                trophyObservable.AddСaller(enemy);
            };


            var levelUpBonusSystemController =
                new LevelUpBonusSystemController(player, data.LevelUpBonusSystemData, uiController.mainCanvas);
            controllers.Add(levelUpBonusSystemController);

            var weaponLevelingSystemController =
                new WeaponLevelingSystemController(player, data.WeaponLevelingSystemData, uiController.mainCanvas);
            controllers.Add(weaponLevelingSystemController);


            spawnController.SpawnEnemies();
        }
    }
}