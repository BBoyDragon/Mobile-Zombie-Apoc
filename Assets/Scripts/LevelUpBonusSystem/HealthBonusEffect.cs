﻿using Enemy;
using UnityEngine;

namespace LevelUpBonusSystem
{
    [CreateAssetMenu(fileName = "HealthBonusEffect", menuName = "BonusEffect/HealthBonusEffect", order = 0)]
    public class HealthBonusEffect : BonusEffect
    {
        [SerializeField]
        private float _healthPoints;
        [SerializeField]
        private string _description;
        public override string Text => $"{_description} {_healthPoints}";
        public override void Apply(Player.Player target)
        {
            target.PlayerStatsManager.HealthManager.ApplyBonusEffect(_healthPoints);
        }
    }
}