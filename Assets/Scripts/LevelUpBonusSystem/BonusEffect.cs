﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace LevelUpBonusSystem
{
    public abstract class BonusEffect : ScriptableObject
    {
        [SerializeField]
        public Sprite Sprite;
        public abstract void Apply(Player.Player target);
        public abstract string Text { get; }
    }
}
