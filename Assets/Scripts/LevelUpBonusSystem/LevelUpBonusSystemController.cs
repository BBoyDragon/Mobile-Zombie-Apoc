﻿using Data;
using Runtime.Controller;
using Singleton;
using UnityEngine;

namespace LevelUpBonusSystem
{
    public class LevelUpBonusSystemController : IInitialization
    {
        private LevelUpBonusManagerView _levelUpBonusManagerView { get;}
        private LevelUpBonusSystemData _levelUpBonusSystemData { get;}

        private LevelUpBonusManagerView _instanse;

        private Transform _parentCanvas;

        public void Init()
        {

        }

        public LevelUpBonusSystemController(Player.Player player, LevelUpBonusSystemData levelUpBonusSystemData, Canvas UICanvas)
        {
            player.OnLevelUp += ActivateBonusSystem;
            _levelUpBonusSystemData = levelUpBonusSystemData;
            _levelUpBonusManagerView = levelUpBonusSystemData.LevelUpBonusManagerView;
            _parentCanvas = UICanvas.transform;
        }
        private void ActivateBonusSystem(Player.Player player)
        {
            EffectController.Instance.ActiveLevelUpPlayerEffect(player.Instance.transform);
            
            TimerHelper.Instance.StartTimer<Player.Player>(CreateLevelUpBonusManager, 1f, player);          
        }

        private void CreateLevelUpBonusManager(Player.Player player)
        {
            _instanse = GameObject.Instantiate(_levelUpBonusManagerView, _parentCanvas);
            _instanse.Init(player);
            _instanse.OnWorkIsDone += DestroyInstanse;
            _instanse.SetBonusCard(_levelUpBonusSystemData.BonusEffects);
        }

        private void DestroyInstanse()
        {
            _instanse.OnWorkIsDone -= DestroyInstanse;
            Object.Destroy(_instanse.gameObject);
        }
    }
}
