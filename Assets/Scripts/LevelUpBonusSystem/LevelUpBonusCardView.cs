using Singleton;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace LevelUpBonusSystem
{
    public class LevelUpBonusCardView : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField]
        private BonusEffect _bonusEffect;
        [SerializeField]
        private Image _panelImage;
        [SerializeField]
        private Color _activeColor;
        [SerializeField]
        private Image _image;
        [SerializeField]
        private TMP_Text _text;

        public System.Action<BonusEffect> OnClick;

        public void SetBonusEffect(BonusEffect bonusEffect)
        {
            _bonusEffect = bonusEffect;
            _text.text = bonusEffect.Text;
            _image.sprite = bonusEffect.Sprite;
        }
        public void OnPointerDown(PointerEventData eventData)
        {
            Debug.Log(this.gameObject.name + " Was Clicked.");
            Debug.Log($"������ ����� {_text.text}");
            _panelImage.color = _activeColor;
            _text.color = Color.black;
            TimerHelper.Instance.StartTimer(InvokeEvent, 0.5f);          
        }

        private void InvokeEvent()
        {
            OnClick?.Invoke(_bonusEffect);
        }

    }
}
