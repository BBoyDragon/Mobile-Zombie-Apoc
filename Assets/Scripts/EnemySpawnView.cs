using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class EnemySpawnView : MonoBehaviour
{
    [SerializeField] private int enemyCount;

    public int EnemyCount => enemyCount;
    public RectTransform RectTransform => GetComponent<RectTransform>();
}
