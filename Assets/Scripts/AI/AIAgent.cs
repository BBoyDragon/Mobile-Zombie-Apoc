﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dialoge_Editor.Runtime;
using UnityEngine;
using UnityEngine.AI;

namespace AI
{
    public class AIAgent
    {
        
        public Enemy.Enemy Enemy { get; }

        public Vector3 TargetPoint;

        public Node StartNode { get; private set; }

        public NavMeshAgent NavMeshAgent { get; }

        public GameObject Target { get; private set; }

        public AIAgent(Enemy.Enemy enemy, NavMeshAgent navMeshAgent)
        {
            Enemy = enemy;
            NavMeshAgent = navMeshAgent;
            NavMeshAgent.speed = Enemy.Data.Speed;
            BuildGraph(enemy.Data.Container);
        }

        private void BuildGraph(AIContainer aiContainer)
        {
            var nodeStack = new Stack<BaseNodeData>();
            aiContainer.NodeDatas.ForEach(node =>
            {
                if (aiContainer.NodeLinks[0].TargetNodeGUID != node.NodeGUID)
                    nodeStack.Push(node);
            });
            
            StartNode = new Node(aiContainer.NodeDatas.Find(node =>
                node.NodeGUID == aiContainer.NodeLinks[0].TargetNodeGUID));

            List<Node> nodes = new List<Node> (nodeStack.Count + 1){ StartNode };
            while (nodeStack.Count > 0)
            {
                nodes.Add(new Node(nodeStack.Pop()));
            }

            foreach (var node in nodes)
            {
                aiContainer.NodeLinks.Where(link => link.BaseNodeGUID == node.Data.NodeGUID).ToList().ForEach(
                    link =>
                    {
                        var transitionCondition = node.Data switch
                        {
                            IfNodeData ifNodeData => ifNodeData.FunctionName,
                            SetStateNodeData _ => "transit",
                            _ => throw new NotImplementedException()
                        };

                        node.AddTransition(nodes.First(nod => nod.Data.NodeGUID == link.TargetNodeGUID),
                            transitionCondition);
                    });
            }
        }

        public void SetTarget(GameObject player)
        {
            Target = player;
        }


        public void SetNewState(State state)
        {
            if (state != Enemy.BotStateMachine.CurrentState)
                Enemy.BotStateMachine.ChangeState(state);
        }

        public void HandleState()
        {
            Enemy.BotStateMachine.CurrentState.LogicUpdate();
            Enemy.BotStateMachine.CurrentState.HandleInput();
            Enemy.BotStateMachine.CurrentState.PhysicsUpdate();
            Enemy.EnemyView.HealthBarView.transform.LookAt(Camera.main.transform); //TODO - перенести отсюда
        }
    }
}