﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Controller;
using Dialoge_Editor.Runtime;
using Unity.AI.Navigation;
using UnityEngine;

namespace AI
{
    
    public class AIManager : IFixedExecute
    {
        
        private List<AIAgent> _agents;
       

        private GameObject _playerInstance;
        private Dictionary<AIAgent, float> _distancesToPlayer;

        public AIManager(GameObject playerInstance)
        {
            _distancesToPlayer = new Dictionary<AIAgent, float>();
            _agents = new List<AIAgent>();
            _playerInstance = playerInstance;
           
        }

        
        public void Add(AIAgent agent)
        {
            _distancesToPlayer.Add(agent, -1f);
            agent.Enemy.OnDead += enemy => Remove(enemy.Agent);
            _agents.Add(agent);
        }

        public void Remove(AIAgent agent)
        {
            if (_agents.Contains(agent))
                _agents.Remove(agent);
            if (_distancesToPlayer.ContainsKey(agent))
                _distancesToPlayer.Remove(agent);

            SetDeathState(agent);
        }

        private void SetDeathState(AIAgent agent)
        {
            agent.SetNewState(agent.Enemy.DeathState);
            agent.HandleState();
        }

        private bool DecriptFormula(string formula, AIAgent agent)
        {
            var splitedFormula = formula.Split(' ');
            Regex varRegex = new Regex(@"\{\w*}");
            Regex signRegex = new Regex(@"[><=]");
            string returningStr = "";


            foreach (var sign in splitedFormula)
            {
                MatchCollection varMatches = varRegex.Matches(sign);
                MatchCollection signMatches = signRegex.Matches(sign);
                if (varMatches.Count > 0)
                {
                    var property = agent.Enemy.Data.Container.ExposedProperties.FirstOrDefault(prop =>
                        prop.PropertyName.Equals(varMatches[0].Value.Replace("{", "").Replace("}", "")));
                    if (varMatches[0].Value == "{DistanceToPlayer}")
                    {
                        returningStr += _distancesToPlayer[agent];
                    }
                    else if ( property !=
                              null)
                    {
                        returningStr += property.PropertyValue;
                    }
                }

                if (signMatches.Count > 0)
                {
                    returningStr = signMatches.Cast<Match>()
                        .Aggregate(returningStr, (current, signMatch) => current + signMatch.Value);
                }

                if (signMatches.Count == 0 && varMatches.Count == 0)
                {
                    returningStr += sign;
                }
            }

            returningStr = returningStr.Replace(",", ".");
            var data = new DataTable().Compute(returningStr, "");

            return (bool)data;
        }

        private State GetNewState(AIAgent agent)
        {
            var curentNode = agent.StartNode;
            var prevNode = agent.StartNode;
            do
            {
                prevNode = curentNode;
                switch (curentNode.Data)
                {
                    case IfNodeData ifNodeData:
                        curentNode = DecriptFormula(ifNodeData.FunctionName, agent)
                            ? curentNode.Transition[0].Next
                            : curentNode.Transition[1].Next;
                        break;
                    case SetStateNodeData stateNodeData:
                        agent.SetTarget(_playerInstance);
                        
                        return stateNodeData.States switch
                        {
                            BotState.Patrol => agent.Enemy.PatrolState,
                            BotState.Agressive => agent.Enemy.AgressiveState,
                            BotState.Attack => agent.Enemy.AttackState,
                            _ => throw new NotImplementedException()
                        };
                    

                    default:
                        throw new NotImplementedException();
                }
            } while (prevNode.Transition.Count > 0);

            return null;
        }

        public void FixedExecute()
        {
            _agents.ForEach(agent =>
            {
                _distancesToPlayer[agent] = Vector3.Distance(agent.Enemy.EnemyView.transform.position,
                    _playerInstance.transform.position);

                agent.SetNewState(GetNewState(agent));

                agent.HandleState();
            });
        }
    }
}