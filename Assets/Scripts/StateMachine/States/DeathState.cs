﻿using DefaultNamespace;
using Singleton;
using UnityEngine;


namespace StateMachine.States
{
    public class DeathState : State
    {
        public DeathState(Enemy.Enemy character, StateMachine stateMachine) : base(character, stateMachine)
        {
        }

        public override void Enter()
        {
            base.Enter();
            character.Agent.NavMeshAgent.speed = 0;
            // TODO запустить анимацию и звуковое сопровождение
            //character.EnemyView.GetComponent<Animator>().SetTrigger("Death");
            RemoveColliders(character.EnemyView.GetComponents<Collider>());
            RemoveColliders(character.EnemyView.GetComponentsInChildren<Collider>());
            Debug.Log($"{character.Data.Name} is dead");
            TimerHelper.Instance.StartTimer(RemoveGameObject, 20f);
        }

        void RemoveColliders(Collider[] colliders)
        {
            foreach (Collider collider in colliders)
            {
                collider.enabled = false;
            }
        }

        void RemoveGameObject()
        {
            Object.Destroy(character.EnemyView.gameObject);
            Debug.Log($"{character.Data.Name} was destroyed");
        }
    }
}
