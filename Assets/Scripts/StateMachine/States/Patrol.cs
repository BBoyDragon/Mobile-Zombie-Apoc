﻿using AI;
using UnityEngine;

namespace StateMachine.States
{
    public class Patrol : State
    {
        public Patrol(Enemy.Enemy character, StateMachine stateMachine) : base(character, stateMachine)
        {
        }

        public override void Enter()
        {
            base.Enter();
            
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            character.Agent.NavMeshAgent.speed = character.Agent.Enemy.Data.Speed;
            var targetPoint = character.Agent.Target.transform.position;
            character.Agent.NavMeshAgent.SetDestination(targetPoint + new Vector3(0, 1, 0));
            character.Agent.TargetPoint = targetPoint + new Vector3(0, 1, 0);
        }

        public override void Exit()
        {
            base.Exit();
        }
    }
}