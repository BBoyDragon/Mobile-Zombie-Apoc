﻿using DefaultNamespace;
using Enemy;
using Singleton;
using UnityEngine;

namespace StateMachine.States
{
    public class AttackState : State
    {
        private Coroutine attackCooldownCoroutine;

        public AttackState(Enemy.Enemy character, StateMachine stateMachine) : base(character, stateMachine)
        {
        }

        public override void Enter()
        {
            base.Enter();
            character.Agent.Target.GetComponent<IDamagable>().Hit(character.Data.Dmg);
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            character.Agent.NavMeshAgent.speed = 0;
            attackCooldownCoroutine ??= TimerHelper.Instance.StartTimer(
                () =>
                {
                    character.Agent.Target.GetComponent<IDamagable>().Hit(character.Data.Dmg);
                    attackCooldownCoroutine = null;
                },
                character.Data.AttackCoooldown);
        }

        public override void Exit()
        {
            base.Exit();
            if (attackCooldownCoroutine != null)
                TimerHelper.Instance.StopTimer(attackCooldownCoroutine);
        }
    }
}