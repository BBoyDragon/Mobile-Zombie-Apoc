﻿using UnityEngine;

namespace StateMachine.States
{
    public class AgressiveState : State
    {
        public AgressiveState(Enemy.Enemy character, StateMachine stateMachine) : base(character, stateMachine)
        {
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            character.Agent.NavMeshAgent.speed = character.Data.AttackSpeed;
            var targetPosition = character.Agent.Target.transform.position;
            character.Agent.NavMeshAgent.SetDestination(targetPosition + new Vector3(0, 1, 0));
            character.Agent.TargetPoint = targetPosition + new Vector3(0, 1, 0);
        }
    }
}