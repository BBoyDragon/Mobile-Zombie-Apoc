using UnityEngine;

namespace StatsManager
{
    public class DodgeChanceManager
    {
        public float DodgeChance => _dodgeChance;

        private float _dodgeChance;

        public DodgeChanceManager(float dodgeChance)
        {
            _dodgeChance = dodgeChance;
        }

        public void ModifyDodgeChance(float amount)
        {
            _dodgeChance += amount;
            _dodgeChance = Mathf.Clamp(_dodgeChance, 0, 1);
        }
    }
}
