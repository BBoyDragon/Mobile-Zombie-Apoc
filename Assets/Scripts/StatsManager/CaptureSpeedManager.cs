namespace StatsManager
{
    public class CaptureSpeedManager
    {
        public float TargetCaptureSpeed => _targetCaptureSpeed;

        private float _targetCaptureSpeed;

        public CaptureSpeedManager(float targetCaptureSpeed)
        {
            _targetCaptureSpeed = targetCaptureSpeed;
        }

        public void ModifyCaptureSpeed(float amount)
        {
            _targetCaptureSpeed += amount;
        }
    }
}
