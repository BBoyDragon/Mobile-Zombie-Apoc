using UnityEngine;

namespace StatsManager
{
    public class CritManager
    {
        public float CritChance => _critChance;
        public float CritPower => _critPower;

        private float _critChance;
        private float _critPower;

        public CritManager(float critChance, float critPower)
        {
            _critChance = critChance;
            _critPower = critPower;
        }

        public void ModifyCritChance(float amount)
        {
            _critChance += amount;
            _critChance = Mathf.Clamp(_critChance, 0, 1);
        }

        public void ModifyCritPower(float amount)
        {
            _critPower += amount;
        }
    }
}
