using UnityEngine;

namespace StatsManager
{
    public class AccuracyManager
    {
        public float Accuracy => _accuracy;

        private float _accuracy;

        public AccuracyManager(float accuracy)
        {
            _accuracy = accuracy;
        }

        public void ModifyAccuracy(float amount)
        {
            _accuracy += amount;
            _accuracy = Mathf.Clamp(_accuracy, 0, 1);
        }
    }
}
