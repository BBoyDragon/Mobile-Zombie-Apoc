namespace StatsManager
{
    public class ArmorManager
    {
        public int Armor => _armor;

        private int _armor;

        public ArmorManager(int armor)
        {
            _armor = armor;
        }

        public void ModifyAccuracy(int amount)
        {
            _armor += amount;
        }
    }
}