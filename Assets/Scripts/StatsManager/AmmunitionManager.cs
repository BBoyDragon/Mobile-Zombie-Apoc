namespace StatsManager
{
    public class AmmunitionManager
    {
        public int Ammunition => _ammunition;

        private int _ammunition;

        public AmmunitionManager(int ammunition)
        {
            _ammunition = ammunition;
        }

        public void ModifyAmmunition(int amount)
        {
            _ammunition += amount;
        }
    }
}
