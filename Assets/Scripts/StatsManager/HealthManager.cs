﻿using System;
using DefaultNamespace;
using DG.Tweening;
using UI;
using UnityEngine;

namespace StatsManager
{
    public class HealthManager : Enemy.IHealthManager
    {
        public float Health => _health;
        public bool IsDead => _isDead;

        private float _health;
        private bool _isDead;
        private ProgressbarView _healthBarView;
        private float _maxHealth;
        public event Action OnDead;

        public HealthManager(float health, ProgressbarView healthBarView)
        {
            _health = health;
            _maxHealth = health;
            _healthBarView = healthBarView;
            _healthBarView.ProgressBar.DOFillAmount(1, 0);
        }

        public void ApplyDamage(float damage)
        {
            if (_isDead)
                return;

            _health -= damage;
            UpdateProgressBar();

            if (_health <= 0)
            {
                _isDead = true;
                OnDead?.Invoke();
            }
        }

        public void ApplyBonusEffect(float healthBonus)
        {
            _maxHealth += healthBonus;
            UpdateProgressBar();
            Debug.Log("Бонус здоровья применен");
        }

        private void UpdateProgressBar()
        {
            _healthBarView.ProgressBar.DOFillAmount(_health / _maxHealth, 0.3f);
        }

    }
}
