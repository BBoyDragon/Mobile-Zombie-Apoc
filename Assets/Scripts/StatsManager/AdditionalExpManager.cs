namespace StatsManager
{
    public class AdditionalExpManager
    {
        public float AdditionalExperience => _additionalExperience;

        private float _additionalExperience;

        public AdditionalExpManager(float additionalExperience)
        {
            _additionalExperience = additionalExperience;
        }

        public void ModifyAditionalExp(float amount)
        {
            _additionalExperience += amount;
        }
    }
}
