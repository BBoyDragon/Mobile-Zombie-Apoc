namespace StatsManager
{
    public class SpeedManager
    {
        public float Speed => _speed;

        private float _speed;

        public SpeedManager(float speed)
        {
            _speed = speed;
        }

        public void ModifySpeed(float amount)
        {
            _speed += amount;
        }
    }
}
