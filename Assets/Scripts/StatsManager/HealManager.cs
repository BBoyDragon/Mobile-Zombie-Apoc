using UnityEngine;

namespace StatsManager
{
    public class HealManager
    {
        public float HealingGain => _healingGain;
        public float HealthRegeneration => _healthRegeneration;

        private float _healthRegeneration;
        private float _healingGain;

        public HealManager(float healthRegeneration, float healingGain)
        {
            _healthRegeneration = healthRegeneration;
            _healingGain = healingGain;
        }

        public void ModifyHealingGain(float amount)
        {
            _healingGain += amount;
        }

        public void ModifyHealthRegeneration(float amount)
        {
            _healthRegeneration += amount;
            _healthRegeneration = Mathf.Clamp(_healthRegeneration, 0, 1);
        }
    }
}
