namespace StatsManager
{
    public class ReloadSpeedManager
    {
        public float ReloadSpeed => _reloadSpeed;

        private float _reloadSpeed;

        public ReloadSpeedManager(float reloadSpeed)
        {
            _reloadSpeed = reloadSpeed;
        }

        public void ModifyReloadSpeed(float amount)
        {
            _reloadSpeed += amount;
        }
    }
}
