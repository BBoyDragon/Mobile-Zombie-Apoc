﻿using System;
using Enemy;
using UnityEngine;

namespace FogOfWar
{
    [RequireComponent(typeof(Collider)) ]
    public class ViewAreaView: MonoBehaviour, IViewAreaView
    {
        public event Action<EnemyView> OnEnemyEnter;
        public event Action<EnemyView> OnEnemyExit;
        
        private void OnTriggerEnter(Collider other)
        {
            if (!other.TryGetComponent<EnemyView>(out var enemyView))
                return;

            OnEnemyEnter?.Invoke(enemyView);
        }

        private void OnTriggerExit(Collider other)
        {
            if (!other.TryGetComponent<EnemyView>(out var enemyView))
                return;

            OnEnemyExit?.Invoke(enemyView);
        }
    }
}