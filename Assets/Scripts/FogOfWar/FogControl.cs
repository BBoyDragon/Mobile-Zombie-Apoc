﻿using Data;
using DefaultNamespace;
using Enemy;
using Player;
using Runtime.Controller;
using UnityEngine;

namespace FogOfWar
{
    public class FogControl: ICleanup, IExecute
    {
        private static readonly string PlayerLayerMask = "Player";
        private static readonly int PlayerPos = Shader.PropertyToID("_Player_Pos");
        private static readonly int RaycastDistance = 1000;
        
        private FogOfWarData _fogOfWarData;
        
        private FogView _fogView;
        private IViewAreaView viewAreaView;

        private Transform _playerTransform;
        private Camera _camera;

        public FogControl(FogOfWarData fogOfWarData, GameObject playerGameObject, Camera camera)
        {
            _fogOfWarData = fogOfWarData;
            _playerTransform = playerGameObject.transform;
            _camera = camera;
        }
        
        public void Spawn(Transform spawnPosition, Transform parent)
        {
            if (_fogOfWarData.ViewAreaEnabled)
            {
                var fieldOfViewGO = Object.Instantiate(
                    _fogOfWarData.ViewAreaPrefab, 
                    spawnPosition.position, 
                    spawnPosition.rotation);
                fieldOfViewGO.transform.parent = parent;
           
                viewAreaView = fieldOfViewGO.GetOrAddComponent<ViewAreaView>();
                viewAreaView.OnEnemyEnter += OnEnemyEnter;
                viewAreaView.OnEnemyExit += OnEnemyExit;
            }
            
            if (_fogOfWarData.FogEnabled)
            {
                var fogGO = Object.Instantiate(
                    _fogOfWarData.FogPrefab, 
                    spawnPosition.position + _fogOfWarData.FogPrefab.transform.position, 
                    spawnPosition.rotation);

                _fogView = fogGO.GetComponent<FogView>();
                _fogView.SetSize(_fogOfWarData.FogWidth, _fogOfWarData.FogLength);
                _fogView.SetSegmentsSize(_fogOfWarData.FogSegmentsWidth, _fogOfWarData.FogSegmentsLength);
                _fogView.SetupMaterial(_fogOfWarData.FogColor, _fogOfWarData.FogViewAreaRadius, 
                    _fogOfWarData.FogViewAreaBorderSize);
                _fogView.BuildPlane();
            }
        }
        
        public void Execute()
        {
            if (!_fogOfWarData.FogEnabled)
                return;
            
            var screenPos = _camera.WorldToScreenPoint(_playerTransform.position);
            var rayToPlayerPos = _camera.ScreenPointToRay(screenPos);

            if (Physics.Raycast(rayToPlayerPos, out var hit, RaycastDistance, LayerMask.GetMask(PlayerLayerMask))) 
            {
                _fogView.Renderer.material.SetVector(PlayerPos, hit.point);
            }
        }

        public void Cleanup()
        {
            if (!_fogOfWarData.ViewAreaEnabled)
                return;
            
            viewAreaView.OnEnemyEnter -= OnEnemyEnter;
            viewAreaView.OnEnemyExit -= OnEnemyExit;
        }

        private void OnEnemyEnter(EnemyView enemyView)
        {
            enemyView.SetVisible(true);
        }

        private void OnEnemyExit(EnemyView enemyView)
        {
            enemyView.SetVisible(false);
        }
    }
}