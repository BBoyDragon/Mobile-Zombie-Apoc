﻿using UnityEngine;

namespace FogOfWar
{
    public interface IFogView
    {
        public Renderer Renderer { get; }
        
        public void SetupMaterial(Color color, float radius, float borderSize);
        public void SetSegmentsSize(int widthSegments, int lengthSegments);
        public void SetSize(float width, float length);
        
        public void BuildPlane();
    }
}