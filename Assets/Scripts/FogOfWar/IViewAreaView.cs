﻿using System;
using Enemy;

namespace FogOfWar
{
    public interface IViewAreaView
    {
        public event Action<EnemyView> OnEnemyEnter;
        public event Action<EnemyView> OnEnemyExit;
    }
}