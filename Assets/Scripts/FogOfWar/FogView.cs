﻿using System;
using UnityEngine;

namespace FogOfWar
{
    [RequireComponent(typeof(MeshFilter), typeof(Renderer))]
    public class FogView: MonoBehaviour, IFogView
    {
        private static readonly int FogRadius = Shader.PropertyToID("_FogRadius");
        private static readonly int FogBorderSize = Shader.PropertyToID("_FogBorderSize");
        
        private Renderer _renderer;
        private MeshFilter _meshFilter;
        
        private int _widthSegments = 1;
        private int _lengthSegments = 1;
        
        private float _width = 1.0f;
        private float _length = 1.0f;

        private Color _color;
        private float _radius;
        private float _borderSize;

        public Renderer Renderer => _renderer;

        private void Start()
        {
            _renderer = GetComponent<Renderer>();
            _renderer.material.SetFloat(FogRadius, _radius);
            _renderer.material.SetFloat(FogBorderSize, _borderSize);
            _renderer.material.color = _color;
        }

        public void SetupMaterial(Color color, float radius, float borderSize)
        {
            _color = color;
            _radius = radius;
            _borderSize = borderSize;
        }

        public void SetSegmentsSize(int widthSegments, int lengthSegments)
        {
            _widthSegments = widthSegments;
            _lengthSegments = lengthSegments;
            
            _widthSegments = Mathf.Clamp(_widthSegments, 1, 254);
            _lengthSegments = Mathf.Clamp(_lengthSegments, 1, 254);
        }

        public void SetSize(float width, float length)
        {
            _width = width;
            _length = length;
        }

        public void BuildPlane()
        {
            transform.localScale = Vector3.one;
            
            _meshFilter = GetComponent<MeshFilter>();
            
            var anchorOffset = Vector2.zero;

            var hCount2 = _widthSegments + 1;
            var vCount2 = _lengthSegments + 1;
            
            var numTriangles = _widthSegments * _lengthSegments * 6;
            var numVertices = hCount2 * vCount2;
            
            var triangles = new int[numTriangles];
            var vertices = new Vector3[numVertices];
            var uvs = new Vector2[numVertices];

            var index = 0;
            var uvFactorX = 1.0f / _widthSegments;
            var uvFactorY = 1.0f / _lengthSegments;
            var scaleX = _width / _widthSegments;
            var scaleY = _length / _lengthSegments;
            
            for (var y = 0.0f; y < vCount2; y++)
            {
                for (var x = 0.0f; x < hCount2; x++)
                {
                    vertices[index] = new Vector3(x * scaleX - _width / 2f - anchorOffset.x, 0.0f, y * scaleY - _length / 2f - anchorOffset.y);
                    uvs[index++] = new Vector2(x * uvFactorX, y * uvFactorY);
                }
            }

            index = 0;
            for (var y = 0; y < _lengthSegments; y++)
            {
                for (var x = 0; x < _lengthSegments; x++)
                {
                    triangles[index]   = (y     * hCount2) + x;
                    triangles[index+1] = ((y+1) * hCount2) + x;
                    triangles[index+2] = (y     * hCount2) + x + 1;

                    triangles[index+3] = ((y+1) * hCount2) + x;
                    triangles[index+4] = ((y+1) * hCount2) + x + 1;
                    triangles[index+5] = (y     * hCount2) + x + 1;
                    index += 6;
                }
            }

            var mesh = new Mesh();
            mesh.name = name;
            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.uv = uvs;

            mesh.RecalculateNormals();
            _meshFilter.mesh = mesh;
            mesh.RecalculateBounds();
        }
    }
}