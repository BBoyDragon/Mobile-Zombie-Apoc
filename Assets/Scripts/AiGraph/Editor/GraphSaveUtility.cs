﻿using System.Collections.Generic;
using System.Linq;
using AiGraph.Editor.Graph;
using AiGraph.Editor.Nodes;
using Dialoge_Editor.Runtime;
using Subtegral.DialogueSystem.DataContainers;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace AiGraph.Editor
{
    public class GraphSaveUtility
    {
        private List<Edge> Edges => _graphView.edges.ToList();
        private List<BaseNode> Nodes => _graphView.nodes.ToList().Cast<BaseNode>().ToList();

        private List<Group> CommentBlocks =>
            _graphView.graphElements.ToList().Where(x => x is Group).Cast<Group>().ToList();

        private AIContainer _aiContainer;
        private AiGraphView _graphView;

        public static GraphSaveUtility GetInstance(AiGraphView graphView)
        {
            return new GraphSaveUtility
            {
                _graphView = graphView
            };
        }

        public void SaveGraph(string fileName)
        {
            var dialogueContainerObject = ScriptableObject.CreateInstance<AIContainer>();
            if (!SaveGraph(dialogueContainerObject)) return;
            SaveExposedProperties(dialogueContainerObject);
            SaveCommentBlocks(dialogueContainerObject);

            if (!AssetDatabase.IsValidFolder("Assets/Resources"))
                AssetDatabase.CreateFolder("Assets", "Resources");

            var loadedAsset =
                AssetDatabase.LoadAssetAtPath($"Assets/Resources/{fileName}.asset", typeof(AIContainer));

            if (loadedAsset == null || !AssetDatabase.Contains(loadedAsset))
            {
                AssetDatabase.CreateAsset(dialogueContainerObject, $"Assets/Resources/{fileName}.asset");
            }
            else
            {
                if (loadedAsset is AIContainer container)
                {
                    container.NodeLinks = dialogueContainerObject.NodeLinks;
                    container.NodeDatas = dialogueContainerObject.NodeDatas;
                    container.ExposedProperties = dialogueContainerObject.ExposedProperties;
                    container.CommentBlockData = dialogueContainerObject.CommentBlockData;
                    EditorUtility.SetDirty(container);
                }
            }

            AssetDatabase.SaveAssets();
        }

        //тут добавление в список в контейнере 
        private void SaveNode(AIContainer aiContainerObject)
        {
            var dialogeNodes = Nodes.OfType<SetStateNode>().ToList();
            foreach (var node in dialogeNodes)
            {
                aiContainerObject.NodeDatas.Add(new SetStateNodeData
                {
                    NodeGUID = node.Guid,
                    States = node.States,
                    Position = node.GetPosition().position,
                    isStartNode = node.EntryPoint,
                    
                });
            }

            Nodes.OfType<IfNode>().ToList().ForEach(node =>
            {
                aiContainerObject.NodeDatas.Add(new IfNodeData
                {
                    NodeGUID = node.Guid,
                    FunctionName = node.FunctionName,
                    Position = node.GetPosition().position,
                    isStartNode = node.EntryPoint
                });
            });
        }


        private bool SaveGraph(AIContainer aiContainerObject)
        {
            if (!Edges.Any()) return false;
            var connectedSockets = Edges.Where(x => x.input.node != null).ToArray();
            for (var i = 0; i < connectedSockets.Count(); i++)
            {
                if ((connectedSockets[i].input.node is BaseNode inputNode) &&
                    (connectedSockets[i].output.node is BaseNode outputNode))
                    aiContainerObject.NodeLinks.Add(new NodeLinkData
                    {
                        BaseNodeGUID = outputNode.Guid,
                        PortName = connectedSockets[i].output.portName,
                        TargetNodeGUID = inputNode.Guid
                    });
            }

            SaveNode(aiContainerObject);

            return true;
        }

        private void SaveExposedProperties(AIContainer aiContainer)
        {
            aiContainer.ExposedProperties.Clear();
            aiContainer.ExposedProperties.AddRange(_graphView.ExposedProperties);
        }

        private void SaveCommentBlocks(AIContainer aiContainer)
        {
            foreach (var block in CommentBlocks)
            {
                var nodes = block.containedElements.Where(x => x is SetStateNode).Cast<SetStateNode>()
                    .Select(x => x.Guid)
                    .ToList();

                aiContainer.CommentBlockData.Add(new CommentBlockData
                {
                    ChildNodes = nodes,
                    Title = block.title,
                    Position = block.GetPosition().position
                });
            }
        }

        public void LoadGraph(string fileName)
        {
            _aiContainer = Resources.Load<AIContainer>(fileName);
            if (_aiContainer == null)
            {
                EditorUtility.DisplayDialog("File Not Found", "Target Narrative Data does not exist!", "OK");
                return;
            }

            
            ClearGraph();
            GenerateNodes();
            ConnectNodes();
            AddExposedProperties();
            GenerateCommentBlocks();
        }

        /// <summary>
        /// Set Entry point GUID then Get All Nodes, remove all and their edges. Leave only the entrypoint node. (Remove its edge too)
        /// </summary>
        private void ClearGraph()
        {
            Nodes.Find(x => x.EntryPoint).Guid = _aiContainer.NodeLinks[0].BaseNodeGUID;
            foreach (var perNode in Nodes.Where(perNode => !perNode.EntryPoint))
            {
                Edges.Where(x => x.input.node == perNode).ToList()
                    .ForEach(edge => _graphView.RemoveElement(edge));
                _graphView.RemoveElement(perNode);
            }
        }

        /// <summary>
        /// Create All serialized nodes and assign their guid and dialogue text to them
        /// </summary>
        private void GenerateNodes()
        {
            foreach (var perNode in _aiContainer.NodeDatas.Where(x => !x.isStartNode))
            {
                var tempNode = _graphView.CreateNode(perNode, Vector2.zero);
                tempNode.Guid = perNode.NodeGUID;
                _graphView.AddElement(tempNode);

                var nodePorts = _aiContainer.NodeLinks.Where(x => x.BaseNodeGUID == perNode.NodeGUID).ToList();
                if (tempNode.NeedGeneratePorts)
                    nodePorts.ForEach(x => _graphView.AddChoicePort(tempNode, x.PortName));
            }
        }

        private void ConnectNodes()
        {
            for (var i = 0; i < Nodes.Count; i++)
            {
                var k = i; //Prevent access to modified closure
                var connections = _aiContainer.NodeLinks.Where(x => x.BaseNodeGUID == Nodes[k].Guid).ToList();
                for (var j = 0; j < connections.Count(); j++)
                {
                    var targetNodeGuid = connections[j].TargetNodeGUID;
                    var targetNode = Nodes.First(x => x.Guid == targetNodeGuid);
                    LinkNodesTogether(Nodes[i].outputContainer[j].Q<Port>(), (Port)targetNode.inputContainer[0]);

                    targetNode.SetPosition(new Rect(
                        _aiContainer.NodeDatas.First(x => x.NodeGUID == targetNodeGuid).Position,
                        _graphView.DefaultNodeSize));
                }
            }
        }

        private void LinkNodesTogether(Port outputSocket, Port inputSocket)
        {
            var tempEdge = new Edge()
            {
                output = outputSocket,
                input = inputSocket
            };
            tempEdge.input.Connect(tempEdge);
            tempEdge.output.Connect(tempEdge);
            _graphView.Add(tempEdge);
        }

        private void AddExposedProperties()
        {
            _graphView.ClearBlackBoardAndExposedProperties();
            foreach (var exposedProperty in _aiContainer.ExposedProperties)
            {
                _graphView.AddPropertyToBlackBoard(exposedProperty);
            }
        }

        private void GenerateCommentBlocks()
        {
            foreach (var commentBlock in CommentBlocks)
            {
                _graphView.RemoveElement(commentBlock);
            }

            foreach (var commentBlockData in _aiContainer.CommentBlockData)
            {
                var block = _graphView.CreateCommentBlock(
                    new Rect(commentBlockData.Position, _graphView.DefaultCommentBlockSize),
                    commentBlockData);
                block.AddElements(Nodes.Where(x => commentBlockData.ChildNodes.Contains(x.Guid)));
            }
        }
    }
}